//
//  TutorialViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import KVLoading

class TutorialViewController: UIViewController ,UIWebViewDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let pdf = Bundle.main.url(forResource: "manual", withExtension: "pdf", subdirectory: nil, localization: nil)  {
            let req = NSURLRequest(url: pdf)
            self.webview.delegate = self;
            self.webview.loadRequest(req as URLRequest)
            self.webview.scalesPageToFit = true;
        }
        // Do any additional setup after loading the view.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        KVLoading.show()
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        KVLoading.hide()
    }

    @IBOutlet weak var webview: UIWebView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchBack(_ sender: Any) {
        
        if(self.navigationController != nil)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else{
        self.dismiss(animated: true) {
            
        }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
