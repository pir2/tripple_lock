//
//  CustomDialogViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 7/4/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit

class CustomDialogViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.desc.text = descString.html2String
        // Do any additional setup after loading the view.
    }
    
    var titleString:String = "";
    var descString:String = ""
    var iconName:String = ""

    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var icon: UIImageView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}
extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8), options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}



