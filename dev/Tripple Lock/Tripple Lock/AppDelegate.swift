//
//  AppDelegate.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/10/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SideMenu
import Fabric
import Crashlytics
import JSSAlertView

import Firebase
import RxCocoa
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var user:UserDefaults?
    var passcode:String?
    
    var profileData:NSDictionary?
    var homeView:HomeViewController?
    var currentView:UIViewController?
    
    var wantToLogin:Bool = false
    
    var onResponseValue = Variable<Any>("")
    var disposeBag = DisposeBag()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FIRApp.configure()
        
          IQKeyboardManager.sharedManager().enable = true
        
              Fabric.with([Crashlytics.self])
        
        UserDefaults.resetStandardUserDefaults();
        
        if UserDefaults.standard.object(forKey: "passcode") == nil {
            UserDefaults.standard.setValue("", forKey: "passcode")
        }
        user =  UserDefaults.standard;
        
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuAnimationBackgroundColor = UIColor.black
        SideMenuManager.menuAnimationFadeStrength = 0.66
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let leftMenu = storyboard.instantiateViewController(withIdentifier: "step_side_menu") as! UIViewController
        // Define the menus
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: leftMenu )
        menuLeftNavigationController.leftSide = true

        
        SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
        
        
//            {
//                "result": {
//                    "status": "error",
//                    "error": {
//                        "message": {
//                            "code": "device_token_error",
//                            "status_code": "505",
//                            "message": "มีการเข้าใช้แอพพลิเคชั่นบนอุปกรณ์อื่น หากต้องการเข้าใช้แอพพลิเคชั่นบนอุปกรณ์นี้ กรุณาเข้าสู่ระบบอีกครั้ง"
//                        }
//                    }
//                }
//        }
        
        let ob = onResponseValue.asObservable();
        ob.subscribe(onNext: { (response) in
            let dict = response as? [String: AnyObject]
            
//            print(dict?["result"]!["status"])
            
            if(dict?["result"] != nil)
            {
            
            if(dict?["result"]!["error"] != nil)
            {
            
           let r = dict?["result"] as! NSDictionary
                let status = r["status"] as? String;
            
            if(status == "error")
            {
                 let errorO = r["error"] as! NSDictionary
                
                let message =  errorO["message"]
            if( message  is String)
            {

            }
            else{

                do{
                      let errorObj = errorO["message"] as! NSDictionary
                    if( (errorObj["status_code"] as? String) == "505")
                    {

                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {

                            UserDefaults.standard.removeObject(forKey: "token")
                            UserDefaults.standard.synchronize();

                            let viewController =  UIApplication.shared.keyWindow?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "welcome")
                            UIApplication.shared.keyWindow?.rootViewController = viewController
                        })
//
                    }
                }catch {

                }
                }
            }
            }
            }
            
        }, onError: { (Error) in
            
        }, onCompleted: {
            
        }).addDisposableTo(disposeBag)

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
       
        let token = UserDefaults.standard.string(forKey: "token")
    
        if (token != nil)
        {
            print (self.passcode ?? "passcode")
            let vc = self.window?.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "PassCodeView") as! PasscodeViewController
            vc.isOneTimeMode = true;
            vc.authStep = 0;
            vc.askOldPass = false;
            vc.requireBacktoLogout = true;
            
            self.currentView?.present(vc, animated: true, completion: nil)
//            let root = self.window?.rootViewController
//            let views = root?.childViewControllers;
//            let lastChild = views?[(views?.count)!-1] as! ViewController
//           
//            lastChild.present(vc, animated: true, completion: nil)
        }
        

        
                 // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

