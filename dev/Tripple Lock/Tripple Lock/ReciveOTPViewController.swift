//
//  ReciveOTPViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 5/23/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

protocol ReceiveOTPDelegate {
    
     func successOTP()
     func failedOTP()
}

import Alamofire
import RxSwift
import RxAlamofire
import UIKit
import KVLoading


class ReciveOTPViewController: UIViewController,UITextFieldDelegate {
    
    var delegate:ReceiveOTPDelegate? = nil
    let disposeBag = DisposeBag()
    var isRegister:Bool = false
    var refCode:String!

    @IBOutlet weak var ReceiveLabel: UILabel!
    @IBOutlet weak var refLabel: UILabel!
    
    @IBOutlet weak var otpInputtext: UITextField!
    
    @IBAction func touchBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
    
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 6
        
    }
    
    @IBAction func TouchResend(_ sender: Any) {
//        let alert = UIAlertController(title: "กสทช", message: "จัดส่ง OTP เรียบร้อยแล้ว", preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "ตกลง", style: UIAlertActionStyle.default, handler: nil))
//        self.present(alert, animated: true, completion: nil);
        if(isRegister)
        {
            self.resendOTPRegister()
        }else{
            self.resendOTPForgetPassword()
        }
    }
    
    func resendOTPForgetPassword()
    {
        KVLoading.show()
        
        let personalIDString:String = UserDefaults.standard.string(forKey: "personalID")!
        let phone :String = UserDefaults.standard.string(forKey: "phone_otp")!
        
            RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"forgot_password_otp","id":personalIDString,"mobile":phone,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
                .debug()
                .subscribe(onNext: { [weak self] (r, json) in
                    
                    KVLoading.hide()
                    
                    let app = UIApplication.shared.delegate as! AppDelegate
                    app.onResponseValue.value = json
                    
                    if let dict = json as? [String: AnyObject] {
                        
                        let status = dict["result"]!["status"] as! String
                        if(status == "error")
                        {
                            let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                            let message:String = (error["message"]  as? String)!
                            let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                                alert.dismiss(animated: true, completion: nil);
                            }))
                            self?.present(alert, animated:true, completion: nil)
                        }else{
                            
                            let message:String =  dict["result"]!["message"] as! String
                            let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
//                                self?.performSegue(withIdentifier: "otp", sender: nil)
                                
                            }))
                            self?.present(alert, animated:true, completion: {
                                
                            })
                            
                            let dataOTP = dict["result"]!["data"]! as! NSDictionary
                            self?.refCode = dataOTP["reference"] as! String
                            
                            if (self?.refCode != nil) {
                                self?.refLabel.isHidden =  false
                                self?.refLabel.text = "Ref: " + (self?.refCode)!
                            }else{
                                self?.refLabel.isHidden =  true
                            }
                            
                        }
                    }
                    }, onError: { [weak self] (error) in
                        
                })
                .addDisposableTo(disposeBag)
        

    }
    
    func resendOTPRegister()
    {
        KVLoading.show()
        
        let personalIDString:String = UserDefaults.standard.string(forKey: "personalID")!
        let phone :String = UserDefaults.standard.string(forKey: "phone_otp")!
        
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"resend_register_otp","id":personalIDString,"mobile":phone,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                let app = UIApplication.shared.delegate as! AppDelegate
                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                    
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = (error["message"]  as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }else{
                        
                        let message:String =  dict["result"]!["message"] as! String
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
//                            self?.performSegue(withIdentifier: "otp", sender: nil)
                        }))
                        self?.present(alert, animated:true, completion: {
                            
                        })
                        
                        let dataOTP = dict["result"]!["data"]! as! NSDictionary
                        self?.refCode = dataOTP["reference"] as! String
                        
                        if (self?.refCode != nil) {
                              self?.refLabel.isHidden =  false
                            self?.refLabel.text = "Ref: " + (self?.refCode)!
                        }else{
                            self?.refLabel.isHidden =  true
                        }

                        
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
        

    }
    
    @IBAction func touchOK(_ sender: Any) {
        self.checkOTP();
    }
    
    let wrongOTPAmountMax:Int  = 3;
    var wrongOTPAmount:Int  = 0;
    
    func checkOTP()
    {
        
        
        
        
        let porsonalID : String = UserDefaults.standard.string(forKey: "personalID")! as! String;
        let otp :String = (otpInputtext.text as? String)!
         let phone :String = UserDefaults.standard.string(forKey: "phone_otp")!
        let params = ["action":"check_otp","id":porsonalID ?? "","mobile_no":phone,"otp":otp,"device_token":UIDevice.current.identifierForVendor!.uuidString] as [String : Any]
        
        
        KVLoading.show()
        
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: params )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                let app = UIApplication.shared.delegate as! AppDelegate
                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        self?.otpInputtext.text = "";
                        self?.wrongOTPAmount += 1
                        
                        if(self?.wrongOTPAmount == self?.wrongOTPAmountMax)
                        {
                            let message:String = "คุณกรอก OTP ผิดเกิน 3 ครั้ง "
                            let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                                alert.dismiss(animated: true, completion: {
                                    
                                })
                            }))
                            alert.addAction(UIAlertAction.init(title: "ขอรหัส OTP ใหม่อีกครั้ง", style: .default, handler: { (action) in
                                alert.dismiss(animated: true, completion: {
                                    
                                })
                            }))
                            self?.present(alert, animated:true, completion: nil)
                        }else{
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = (error["message"] as! String)
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                        }))
                        self?.present(alert, animated:true, completion: nil)
                        }
                    }else{
                        
                       self?.delegate?.successOTP();
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let message:String = UserDefaults.standard.string(forKey: "phone_otp")!
        
        
        if(message != nil)
        {
            var mobile_no = message
            let start = mobile_no.index((mobile_no.startIndex), offsetBy: 2);
            let end = mobile_no.index((mobile_no.startIndex), offsetBy: 2 + 4);
//            mobile_no.replaceSubrange(start..<end, with: "-XXX-")
            
            if (self.refCode != nil) {
                 self.refLabel.text = "Ref: " + self.refCode
            }else{
                self.refLabel.isHidden =  true
            }
           
            
            
            self.ReceiveLabel.text = "ที่ได้รับจากเบอร์ " + mobile_no
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
(UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
   
        

}
