//
//  LoginViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAlamofire
import PopupDialog
import JSSAlertView
import KVLoading

class LoginViewController: UIViewController,PasscodeDelegate,UITextFieldDelegate{
    
    let disposebag = DisposeBag()
    func recheckOldPasscode(code: String) -> Bool {
        return true;
    }
    
    @IBOutlet weak var mobile_no: UITextField!
    @IBOutlet weak var personal_id: UITextField!

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if(textField == mobile_no)
        {
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 10
        }else{
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 13
        }
    }
    
    @IBAction func touchBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        (UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        
        mobile_no.rx.text
        .subscribe(onNext: { [unowned self] query in // Here we will be notified of every new value
            print(query ?? "")
        })
            .addDisposableTo(disposebag)
        
               // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func requestToHome()
    {
//        let data =  dict["result"]!["data"] as! NSDictionary
//        let access_token = data["access_token"] as! String
//        
//        UserDefaults.standard.setValue(access_token, forKey: "token");
//        
//        UserDefaults.standard.setValue(self?.personal_id.text, forKey: "personalID")
//        UserDefaults.standard.setValue(self?.mobile_no.text, forKey: "mobile")
//        UserDefaults.standard.synchronize();
//        
//        UserDefaults.standard.synchronize()
    }
    
    
    func requestLogin()
    {
        KVLoading.show()
        
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"check_email_id","id":self.personal_id.text ?? "","mobile_no":self.mobile_no.text ?? "","device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                let app = UIApplication.shared.delegate as! AppDelegate
                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]! ?? "no data")
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = (error["message"]  as? String)!
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "",
                                                            text: message ,
                                                            buttonText:"ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                    

                        
                        
                    }else{
                        
                        UserDefaults.standard.setValue(self?.personal_id.text, forKey: "personalID")
                        
                        let mobileMask:String = (self?.mobile_no.text)!;
                        
                        
                        UserDefaults.standard.setValue(self?.mobile_no.text, forKey: "mobile")
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd/MM/yyyy"
                        dateFormatter.locale = Locale.init(identifier: "th_TH");
                        
                        UserDefaults.standard.setValue(dateFormatter.string(from: Date.init()), forKey: "last_login")
                        
                        UserDefaults.standard.synchronize();
                        self?.performSegue(withIdentifier: "passcode", sender: nil);
                      
                    }
                }
                }, onError: { [weak self] (error) in
                    print(error)
            })
            .addDisposableTo(disposebag)

    }
    
   
        func validateField() -> Bool {
            var result  = true;
            
            if((self.mobile_no.text?.characters.count)! < 10)
            {
                result = false
                
                let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                let alertview = JSSAlertView().show(self,
                                                    title: "เข้าสู่ระบบไม่สำเร็จ",
                                                    text: "กรุณากรอกเบอร์โทรศัพท์\nให้ครบ 10 หลัก" ,
                                                    buttonText:"ปิด",
                                                    iconImage: customIcon)
                
                
                alertview.setTitleFont("Athiti-Medium")
                alertview.setTextFont("Athiti-Medium")
                alertview.setButtonFont("Athiti-Medium")
                alertview.setTextTheme(.dark)
                
            }else  if((self.personal_id.text?.characters.count)! < 13)
            {
                result = false
//                let popup = PopupDialog.init(title: "เข้าสู่ระบบไม่สำเร็จ", message: "กรุณากรอกเลขบัตรประชาชน\nให้ครบ 13 หลัก")
//                popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
//                    popup.dismiss()
//                }))
//                self.present(popup, animated: true, completion: nil)
                
                let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                let alertview = JSSAlertView().show(self,
                                                    title: "เข้าสู่ระบบไม่สำเร็จ",
                                                    text: "กรุณากรอกเลขบัตรประชาชน\nให้ครบ 13 หลัก" ,
                                                    buttonText:"ปิด",
                                                    iconImage: customIcon)
                
                
                alertview.setTitleFont("Athiti-Medium")
                alertview.setTextFont("Athiti-Medium")
                alertview.setButtonFont("Athiti-Medium")
                alertview.setTextTheme(.dark)

                
            }
            
            
            return result;
            
        }
    
    

    @IBAction func touchLogin(_ sender: Any) {
//        requestLogin()
        
        if (personal_id.text != "" && mobile_no.text != "") {
            
            if(validateField()){
            requestLogin()
            }
        }else{
//            let popup = PopupDialog(title:"ข้อมูลไม่ครบ", message:"กรุณากรอกข้อมูลให้ครบ")
//            
//            // Create buttons
//            let buttonOne = CancelButton(title: "ปิด") {
//                
//            }
//            popup.addButtons([buttonOne])
//            
//            // Present dialog
//            self.present(popup, animated: true, completion: nil)
            
            
            let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
            let alertview = JSSAlertView().show(self,
                                                title: "ข้อมูลไม่ครบ",
                                                text: "กรุณากรอกข้อมูลให้ครบ" ,
                                                buttonText:"ปิด",
                                                iconImage: customIcon)
            
            
            alertview.setTitleFont("Athiti-Medium")
            alertview.setTextFont("Athiti-Medium")
            alertview.setButtonFont("Athiti-Medium")
            alertview.setTextTheme(.dark)
        }
        
//
        
//        self.performSegue(withIdentifier: "passcode", sender: nil)
        
    }
//    PasscodeDelegate
    func successPassCode(code : String) {
        self.dismiss(animated: false) {
            
            KVLoading.show()
            
            let params = ["action":"authenticate","id":self.personal_id.text ?? "","mobile_no":self.mobile_no.text ?? "","passcode":code,"device_token":UIDevice.current.identifierForVendor!.uuidString,"device_type":"ios","is_mobile":true] as [String : Any]
            
            RxAlamofire.requestJSON(.post, Config.server_url,parameters: params )
                .debug()
                .subscribe(onNext: { [weak self] (r, json) in
                    
                    KVLoading.hide()
                    
                    let app = UIApplication.shared.delegate as! AppDelegate
                    app.onResponseValue.value = json
                    
                    if let dict = json as? [String: AnyObject] {
                        print(dict["result"]!["data"]! ?? "no data")
                        let status = dict["result"]!["status"] as! String
                        if(status == "error")
                        {
                            let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                            let message:String = (error["message"]  as? String)!
//                            let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
//                            alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
//                                alert.dismiss(animated: true, completion: nil);
//                            }))
//                            self?.present(alert, animated:true, completion: nil)
                            
                            
                            let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                            let alertview = JSSAlertView().show(self!,
                                                                title: "",
                                                                text: message ,
                                                                buttonText:"ปิด",
                                                                iconImage: customIcon)
                            
                            
                            alertview.setTitleFont("Athiti-Medium")
                            alertview.setTextFont("Athiti-Medium")
                            alertview.setButtonFont("Athiti-Medium")
                            alertview.setTextTheme(.dark)
                            
                            
                            
                        }else{
                            
                            let result = dict["result"]!["data"]! as! NSDictionary
                            let access_token = result["access_token"]! as? String
                            
                            UserDefaults.standard.set(code, forKey: "passcode")
                           
                            
                            UserDefaults.standard.setValue(access_token, forKey: "token");
                            UserDefaults.standard.synchronize()
                            
                            
                            self?.performSegue(withIdentifier: "home", sender: Any?.self)
                        }
                    }
                    }, onError: { [weak self] (error) in
                        
                })
                .addDisposableTo(self.disposebag)
        }
        
    }
    func failedPassCode() {
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination
        
        if(vc is PasscodeViewController)
        {
            (vc as! PasscodeViewController).isDubbleAuth = false
            (vc as! PasscodeViewController).authStep = 0;
            (vc as! PasscodeViewController).isSetNewAuth = false
            (vc as! PasscodeViewController).delegate = self
            (vc as! PasscodeViewController).isGetPasscodeOnly = true
            (vc as! PasscodeViewController).isOneTimeUseMode = true
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}
