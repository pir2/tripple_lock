//
//  CheckViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import RxAlamofire
import KVLoading

class HistoryViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tableview: UITableView!
    var operatorLists:NSMutableArray  = []
    
    let disposeBag = DisposeBag()
    
    var profileData : NSDictionary?
    var operatorDatas:NSMutableArray = []
    
    @IBOutlet weak var amountNOLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var personalIDLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        (UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        
        self.operatorLists = NSMutableArray();
        
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.requestData()
        self.requestPhoneData()
        self.getProfile()
    }
    
    func updateProfileView()
    {
        var personalID = self.profileData?.value(forKey: "id") as? String
        let start = personalID?.index((personalID?.startIndex)!, offsetBy: 7);
        let end = personalID?.index((personalID?.startIndex)!, offsetBy: 7 + 5);
        personalID?.replaceSubrange(start!..<end!, with: "XXXXX")
        
        
        self.personalIDLabel.text = personalID
        self.usernameLabel.text = (self.profileData?.value(forKey: "first_name") as? String)! + " " +  (self.profileData?.value(forKey: "last_name") as? String)!
    }
    
    func getProfile()
    {
        
        KVLoading.show()
        
        let token  =   UserDefaults.standard.string(forKey: "token")
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"get_profile","access_token":token ?? "","device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                
                KVLoading.hide()
                
                
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                            
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }else{
                        
                        self?.profileData =  dict["result"]!["data"]! as! NSDictionary
                        
                        self?.updateProfileView()
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
    }
    func requestPhoneData()
    {
        KVLoading.show()
        
        let token  =   UserDefaults.standard.string(forKey: "token")
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"check_list","access_token":token ?? "","device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }else{
                        
                        let operData =  NSMutableArray.init(array: dict["result"]!["data"]! as! NSArray)
                      
                        
                        
                        var totalNumber:Int = 0
                        for i : NSDictionary in (operData as! NSArray as! [NSDictionary])
                        {
                            totalNumber += (i["list"] as! NSArray).count
                        }
                        
                        self?.amountNOLabel.text = String(totalNumber)
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
    }
    
    func requestData()
    {
        KVLoading.show()
        
        let token  =   UserDefaults.standard.string(forKey: "token")
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"get_histories  ","access_token":token ?? "","device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }else{
                        
                        let logs:NSMutableArray = NSMutableArray.init(array: dict["result"]!["data"]! as! NSArray)
                        
                     
                      
                        var lastDay = 0
                        let cal:Calendar = Calendar.current
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        self?.operatorDatas =  NSMutableArray.init()
                        var days:NSMutableArray = NSMutableArray.init()
                        for d:NSDictionary in (logs as! NSArray as! [NSDictionary])
                        {
                            let time = d.object(forKey: "created_at") as! String
                            var dateD:Date = formatter.date(from: time)!
                            
                           
                            
                            if(lastDay != cal.component(.day, from: dateD))
                            {
                                lastDay = cal.component(.day, from: dateD)
                                 days = NSMutableArray.init()
                                self?.operatorDatas.add(days)
                            }
                            days.add(d)
                        }
                       
                        
//                        self?.operatorDatas =  NSMutableArray.init(array: dict["result"]!["data"]! as! NSArray)
                        
                        
                        
                        self?.tableview.reloadData()
                        
//                        var totalNumber:Int = 0
//                        for i : NSDictionary in (self?.operatorDatas as! NSArray as! [NSDictionary])
//                        {
//                            totalNumber += (i["list"] as! NSArray).count
//                        }
                        
//                        self?.amountNOLabel.text = String(totalNumber)
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchBack(_ sender: Any) {
        //        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    //    TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "operatorDetail", sender: operatorDatas[indexPath.row] as! Dictionary<String, AnyObject>)
    }
    //    TAbleviewDataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76;
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 41;
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header:HistoryHeaderTableViewCell = tableview.dequeueReusableCell(withIdentifier: "Header")! as! HistoryHeaderTableViewCell
        
        let gregorianCalendar = Calendar(identifier: .gregorian)
        
        
        let formatter = DateFormatter()
       
     
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = (((self.operatorDatas.object(at: section) as? NSMutableArray)?.object(at: 0) as AnyObject).object(forKey: "created_at") as? String)
        let date = formatter.date(from: dateString!)
        
       
        formatter.dateFormat = "yyyy"
        
        let yearInt = Int(formatter.string(from: date!))! + 543
         formatter.locale = Locale(identifier: "th-TH")
        formatter.dateFormat = "วันที่ dd MMM "
         formatter.calendar = gregorianCalendar
        header.headerText?.text = formatter.string(from: date!) + String( yearInt)
        
        
        return header
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:HistoryRowTableViewCell  = tableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HistoryRowTableViewCell
        
        let dateString = (((self.operatorDatas.object(at: indexPath.section) as? NSMutableArray)?.object(at:indexPath.row) as AnyObject).object(forKey: "created_at") as? String)
        let mobile_no = (((self.operatorDatas.object(at: indexPath.section) as? NSMutableArray)?.object(at:indexPath.row) as AnyObject).object(forKey: "mobile_no") as? String)
        let operator_name = (((self.operatorDatas.object(at: indexPath.section) as? NSMutableArray)?.object(at:indexPath.row) as AnyObject).object(forKey: "provider_name") as? String)
        let message = (((self.operatorDatas.object(at: indexPath.section) as? NSMutableArray)?.object(at:indexPath.row) as AnyObject).object(forKey: "message") as? String)
        
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
         let date = formatter.date(from: dateString!)
        formatter.dateFormat = "เวลา HH:mm"
        
       
        
        cell.phone_no.text = mobile_no
        cell.operatorName.text = operator_name
        cell.message.text = message
        cell.timeLabel.text =  formatter.string(from: date!)
        
        
        return cell
    }
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let  sub = operatorDatas.object(at: section) as? NSMutableArray
        return (sub?.count)!;
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return operatorDatas.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc = segue.destination
        
        if (vc is OperatorDetaileViewController)
        {
            (vc as? OperatorDetaileViewController)?.data = sender as AnyObject
        }
    }
    
    
}

