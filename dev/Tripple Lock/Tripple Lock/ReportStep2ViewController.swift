//
//  ReportStep2ViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/20/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit

class ReportStep2ViewController: UIViewController {

    @IBAction func touchBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func tockOK(_ sender: Any) {
        
        let alertController = UIAlertController(title: "แจ้งเบอร์หาย", message: "ทำรายการเรียบร้อย กรุณาติดต่อศูนย์บริการ", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "ตกลง", style: .default, handler:{action in self.gotoMain() })
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    func gotoMain()
    {
         self.performSegue(withIdentifier: "menu", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
