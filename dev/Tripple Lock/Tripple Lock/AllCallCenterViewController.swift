//
//  AllCallCenterViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 6/14/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit

class AllCallCenterViewController: UIViewController,UISearchBarDelegate {

    
    var tableView:CallCenterTableViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
(UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        
        searchbar.delegate = self
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var searchbar: UISearchBar!
    @IBAction func onTouchBack(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        tableView?.filter(keyword: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        tableView?.filter(keyword: "")
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
