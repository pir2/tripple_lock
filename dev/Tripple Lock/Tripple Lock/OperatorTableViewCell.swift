//
//  OperatorTableViewCell.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 5/24/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit

class OperatorTableViewCell: UITableViewCell {

    @IBOutlet weak var totalNumber: UILabel!
    @IBOutlet weak var operatorName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
      
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data : Dictionary<String  , AnyObject>)
    {
        self.totalNumber.text = data["count"]?.stringValue
        self.operatorName.text = data["provider_name"] as? String
        self.dateLabel.text = data["check_date"] as? String
    }

}
