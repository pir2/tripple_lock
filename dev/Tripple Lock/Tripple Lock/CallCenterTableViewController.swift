//
//  CallCenterTableViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 6/2/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import RxAlamofire
import PopupDialog
import KVLoading

class CallCenterTableViewController: UITableViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    
      let disposeBag = DisposeBag()
    
    var callCenterList:NSMutableArray! = []
    var
    
    callCenterFilterList:NSMutableArray! = []
    
    func requestData()
    {
        KVLoading.show()
        
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"call_centers","device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                let app = UIApplication.shared.delegate as! AppDelegate
                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                   
                    self?.callCenterList =  NSMutableArray.init(array: dict["result"]!["data"]! as! NSArray)
                    self?.callCenterFilterList =  NSMutableArray.init(array: dict["result"]!["data"]! as! NSArray)
                    
                    self?.tableView.reloadData()
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
    }
    
    func filter(keyword:String)
    {
        if(keyword == "" )
        {
            self.callCenterFilterList = self.callCenterList;
             self.tableView.reloadData()
        }else{
            self.callCenterFilterList = [];
            
            for var data in self.callCenterList
            {
                let provider_name:String =  ((data as! NSDictionary)["provider_name"] as? String)!
                if(provider_name.lowercased().range(of: keyword.lowercased()) != nil )
                {
                    self.callCenterFilterList.add(data);
                }
               
            }
            
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
(UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        self.requestData()
        
       
        
        tableView.tableFooterView = UIView.init()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
         (parent as! AllCallCenterViewController).tableView  = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.callCenterFilterList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CallCenterRowTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CallCenterRowTableViewCell

        cell.setData(data: self.callCenterFilterList[indexPath.row] as? NSDictionary)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView.init()
    }
    
   override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data:  NSDictionary = self.callCenterFilterList[indexPath.row] as! NSDictionary
        let provider_name = data["provider_name"]  as? String
//        let  popup = PopupDialog.init(title: "ต้องการติดต่อ", message: provider_name! + " ใช่หรือไม่")
//        
//        popup.addButton(PopupDialogButton.init(title: "โทร", action: {
//            popup.dismiss()
//            
//            let url = URL(string: "tel:" + (data["tel"] as! String))!
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:]){ action in
//                    
//                }
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//
//            
//        }))
//        popup.buttonAlignment = .horizontal
//        popup.addButton(PopupDialogButton.init(title: "ยกเลิก", action: {
//            popup.dismiss()
//        }))
//
//        
//        present(popup, animated: true, completion: nil)
        
        //------------------
        
        let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "callcenter_alert") as! StepAlertViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.titleText = "ต้องการติดต่อ"
        myAlert.messageText = provider_name! + " ใช่หรือไม่"
        myAlert.touchNagative {
            let url = URL(string: "tel:" + (data["tel"] as! String))!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:]){ action in
                    
                }
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
        self.present(myAlert, animated: true, completion: {
            
            
        })
        
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
