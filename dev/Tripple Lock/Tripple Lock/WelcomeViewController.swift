//
//  WelcomeViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import SideMenu
import PopupDialog
import JSSAlertView
import Firebase

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

class WelcomeViewController: UIViewController,PasscodeDelegate {
    
    
    var remoteConfig:FIRRemoteConfig?

    
    var confirmpasscode:Bool = false
    func failedPassCode() {
        
    }
    func successPassCode(code: String) {
        confirmpasscode = true
       
    }
    func recheckOldPasscode(code: String) -> Bool {
        return true
    }
    
 
    
    @IBAction func touchRegister(_ sender: Any) {
        
//        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "success_alert") as! UIViewController
//        customAlert.providesPresentationContextTransitionStyle = true
//        customAlert.definesPresentationContext = true
//        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//        
//        self.present(customAlert, animated: true, completion: nil)
        
        self.performSegue(withIdentifier: "register", sender: self)
    }
    
    @IBAction func touchLogin(_ sender: Any) {
        self.performSegue(withIdentifier: "login", sender: self)
    }
    
    @IBAction func touchAbout(_ sender: Any) {
        self.performSegue(withIdentifier: "about", sender: self)
    }
    
    @IBAction func touchHelp(_ sender: Any) {
        self.performSegue(withIdentifier: "help", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        (UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        
        var dialogAppearance = PopupDialogDefaultView.appearance()
        
        dialogAppearance.backgroundColor      = UIColor.white
        dialogAppearance.titleFont            = UIFont.init(name: "Athiti-Medium", size: 30.0)!
        dialogAppearance.titleColor           = UIColor(rgb: 0x222527)
        dialogAppearance.titleTextAlignment   = .center
        dialogAppearance.messageFont          = UIFont.init(name: "Athiti-Medium", size: 20.0)!
        dialogAppearance.messageColor         = UIColor(rgb: 0x222527)
        dialogAppearance.messageTextAlignment = .center
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.remoteConfig = FIRRemoteConfig.remoteConfig()
        self.remoteConfig?.setDefaultsFromPlistFileName("remote_config")
        
        self.remoteConfig?.fetch(withExpirationDuration: TimeInterval(6), completionHandler: { (status, error) in
            self.remoteConfig?.activateFetched();
            
            
            let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject
            let version = nsObject as? String
            
            
            let remote_version = self.remoteConfig?["version"].stringValue
            let force_update = self.remoteConfig?["force_update"].boolValue
            
            if(version != remote_version)
            {
                
                let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                
                
                
                if (force_update == true)
                {
                    let alertview = JSSAlertView().show(self, title: "อัพเดทแอพพลิเคชั่น",
                                                        text: "กรุณาอัพเดทแอพพลิเคชั่นเป็นเวอร์ชันล่าสุด ก่อนเข้าใช้งาน" ,
                                                        buttonText: "ตกลง",
                                                        iconImage:customIcon)
                    
                    
                    alertview.setTitleFont("Athiti-Medium")
                    alertview.setTextFont("Athiti-Medium")
                    alertview.setButtonFont("Athiti-Medium")
                    alertview.setTextTheme(.dark)
                    alertview.addAction({
                        let url = URL(string: (self.remoteConfig?["update_ios_url"].stringValue)!)!
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    })
                }else{
                    let alertview = JSSAlertView().show(self, title: "อัพเดทแอพพลิเคชั่น",
                                                        text: "กรุณาอัพเดทแอพพลิเคชั่นเป็นเวอร์ชันล่าสุด ก่อนเข้าใช้งาน" ,
                                                        buttonText: "ตกลง",
                                                        cancelButtonText:"ไว้ก่อน",
                                                        iconImage:customIcon)
                    
                    
                    alertview.setTitleFont("Athiti-Medium")
                    alertview.setTextFont("Athiti-Medium")
                    alertview.setButtonFont("Athiti-Medium")
                    alertview.setTextTheme(.dark)
                    alertview.addAction({
                        let url = URL(string: (self.remoteConfig?["update_ios_url"].stringValue)!)!
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    })
                    alertview.addCancelAction({
                         self.checkTerm()
                    })
                }
            }else{
                self.checkTerm()
            }
            
        })
        
        
        
    }
    
    func checkTerm()
    {
        let isAccept = UserDefaults.standard.bool(forKey: "accept_term")
        if(isAccept)
        {
            
            let token = UserDefaults.standard.string(forKey: "token")
            
            UserDefaults.standard.set(true, forKey: "require_checklock")
            UserDefaults.standard.synchronize();
            
            
            
            
            let app =    UIApplication.shared.delegate as! AppDelegate
            
            
            if(token != nil)
            {
                self.view.isHidden = true;
                if(confirmpasscode)
                {
                    self.performSegue(withIdentifier: "home", sender: nil)
                }else if(app.wantToLogin){
                    app.wantToLogin = false;
                    self.performSegue(withIdentifier: "login", sender: self)
                }else{
                    self.performSegue(withIdentifier: "passcode", sender: nil)
                }
                
            }else{
                if(app.wantToLogin){
                    app.wantToLogin = false;
                    self.performSegue(withIdentifier: "login", sender: self)
                }
                
                self.view.isHidden = false;
            }
        }else{
            
            performSegue(withIdentifier: "term", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination
        if(vc is PasscodeViewController)
        {
           ( vc as! PasscodeViewController).delegate  = self
             ( vc as! PasscodeViewController).isOneTimeMode = true;
             ( vc as! PasscodeViewController).authStep = 0;
             ( vc as! PasscodeViewController).askOldPass = false;
             ( vc as! PasscodeViewController).requireBacktoLogout = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

