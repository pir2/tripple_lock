//
//  HomeViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import SideMenu
import RxSwift
import RxAlamofire
import PopupDialog
import JSSAlertView
import PMAlertController
import KVLoading

class HomeViewController: UIViewController  {

    let disposeBag  = DisposeBag()
    
    var profile:NSDictionary? = nil
    
    @IBOutlet weak var mobileNoLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lastLabel: UILabel!
    @IBOutlet weak var lockBTN: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        (UIApplication.shared.delegate as! AppDelegate).homeView = self;
        (UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        // Do any additional setup after
      NotificationCenter.default.addObserver(self, selector: #selector(notificate(notification:)), name: .lockStatus, object: nil)
        
       self.navigationController?.navigationBar.isHidden = true
        
        requestGetLockStatus()
        
        
    }
    
//    @IBAction func onTapMenu(_ sender:Any)
//    {
//        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
//    }
    
    @IBAction func onTouchMenu(_ sender:Any)
    {
        self.sideMenuController?.toggle()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let require_check = UserDefaults.standard.bool( forKey: "require_checklock")
      
        if(require_check)
        {
            UserDefaults.standard.set(false, forKey: "require_checklock")
            UserDefaults.standard.synchronize();
            self.checkFirstLogin()
        }
        
          (UIApplication.shared.delegate as! AppDelegate).currentView = self
          self.profile = (UIApplication.shared.delegate as! AppDelegate).profileData
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificate(notification:)), name: .lockStatus, object: nil)
//        if (self.profile != nil)
//        {
//            renderProfile();
//        }else{
            checkProfile();
//        }
        
        requestGetLockStatus();
        
//        let customDialog = CustomDialogViewController.init(nibName: "CustomDialogViewController", bundle: nil)
//        customDialog.descString = "กรุณาตรวจสอบที่อีเมล์ ที่ท่านได้ลงทะเบียนไว้แล้ว เพื่อยืนยันตัวตน<font color=\"red\"> <span style=color:red;>หากไม่ได้รับอีเมล์ กรุณาตรวจสอบที่ Junk Mail ของท่าน</span></font>"
//        
//        let popup = PopupDialog.init(viewController: customDialog, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: false, completion: nil)
//        
//        popup.addButton(PopupDialogButton.init(title: "ตกลง", action: { 
//            popup.dismiss();
//        }))
//        
//        self.present(popup, animated: true, completion: nil)
        
    }
    
    func notificate(notification: Notification){
        requestGetLockStatus()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(notificate(notification:)), name: .lockStatus, object: nil)
    }
    
    
    func renderProfile()
    {
        nameLabel.text = (self.profile?["first_name"]  as! String ) + " " + (self.profile?["last_name"]  as! String )
        
        UserDefaults.standard.set(nameLabel.text, forKey: "username")
        UserDefaults.standard.synchronize()
        var mobile_no :String = UserDefaults.standard.string(forKey: "mobile")!
//        let myRange = Range<String.index>
//       mobile_no.replacingCharacters(in: 0...3, with: "*")

        let start = mobile_no.index(mobile_no.startIndex, offsetBy: 2);
        let end = mobile_no.index(mobile_no.startIndex, offsetBy: 2 + 4);
//        mobile_no.replaceSubrange(start..<end, with: "-XXXX-")
        
        mobileNoLabel.text = mobile_no
        
       var last_login = UserDefaults.standard.string(forKey: "last_login") as? String ??  " - "
        
        if(last_login == " - ")
        {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateFormatter.locale = Locale.init(identifier: "th_TH");
            UserDefaults.standard.setValue(dateFormatter.string(from: Date.init()), forKey: "last_login")
            UserDefaults.standard.synchronize();

            last_login = dateFormatter.string(from: Date.init())
            
        }
        
        do
        {
        lastLabel.text = "ใช้งานล่าสุด " + last_login
            
            
        }catch is Error {
            
            
            
           
        }
    }
    
    func checkProfile()
    {
        KVLoading.show()
        
        let token  =   UserDefaults.standard.string(forKey: "token")
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"get_profile","access_token":token ?? "","device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                
                KVLoading.hide()
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
                        
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "",
                                                            text:  message ,
                                                            buttonText:"ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.addAction {
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                      

                    }else{
                        
                       self?.profile = dict["result"]!["data"]! as! NSDictionary
                        
                        (UIApplication.shared.delegate as! AppDelegate).profileData = self?.profile;
                        
                        self?.renderProfile()

                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
    }
    
    func requestGetLockStatus()
    {
        let token  =   UserDefaults.standard.string(forKey: "token")
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"get_is_lock","access_token":token ?? "","device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
//                let app = UIApplication.shared.delegate as! AppDelegate
//                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
                        
                        
                       
                        let myAlert = self?.storyboard?.instantiateViewController(withIdentifier: "wraning_alert") as! StepAlertViewController
                        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        myAlert.messageText = message
                        myAlert.touchDismissCallback {
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }
                        
                        self?.present(myAlert, animated: true, completion: nil)
                        
                        
                        
                    }else{
                        
                       let data = dict["result"]!["data"]! as! NSDictionary
                        
                        
                        let is_lock = data["is_lock"] as! Bool
                        
                        if(is_lock)
                        {
                            self?.lockBTN.setImage(UIImage.init(named: "home_lock__btn_unactive"), for: .normal)
                        }else{
                            self?.lockBTN.setImage(UIImage.init(named: "home_unlock__btn_unactive"), for: .normal)
                        }
                        
                        UserDefaults.standard.set(is_lock, forKey: "is_lock")
                        UserDefaults.standard.synchronize();
                        
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
    }
    
    
    func requestLockNumber(is_lock : Bool)
    {
        
        KVLoading.show()
        let token  =   UserDefaults.standard.string(forKey: "token")
        var lockString = "true"
        
        if is_lock != true
        {
            lockString = "false"
        }
        
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"set_is_lock","access_token":token ?? "","is_lock":lockString,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
//                let app = UIApplication.shared.delegate as! AppDelegate
//                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
//                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
//                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
//                             alert.dismiss(animated: true, completion: nil)
//                        }))
//                        self?.present(alert, animated:true, completion: nil)
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "",
                                                            text:  message ,
                                                            buttonText:"ปิด",
                                                            iconImage: customIcon)
                        
                        alertview.addAction {
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)

                        
                    }else{
                        
//                        let  message = dict["result"]!["message"]!
//                        let  popup = PopupDialog.init(title: "ล็อค", message: message as! String)
//                        
//                        popup.buttonAlignment = .horizontal
//                        popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
//                             popup.dismiss()
//                        }))
//                        
//                        self?.present(popup, animated: true, completion: nil)
                        
                        let customIcon = #imageLiteral(resourceName: "lock_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "ล็อค",
//                                                            text:  message as! String ,
                                                            text: "ระบบได้ทำการล็อคการเปิดเบอร์เรียบร้อยแล้ว",
                                                            buttonText:"ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)

                        self?.requestGetLockStatus();
                        
                       
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)

    }
    
    func closeDialog()
    {
        popup.dismiss()
        self.requestLockNumber(is_lock: true)
    }
    
    var countdownTime:Timer!
    
    func updatRemainTime(popups:PopupDialog)
    {

        let vc = self.popup.viewController as! PopupDialogDefaultViewController
//        vc.messageText = "aa"



       self.timeRemain -= 1

        vc.messageText = "ระบบจะทำการล็อคการเปิดเบอร์ใหม่ภายใน " + String(format:"%d",self.timeRemain) + " วินาที เพื่อป้องกันบุคคลอื่นแอบอ้าง นำบัตรประชาชนไปเปิดหมายเลขโทรศัพท์"


    }
    
    var timeRemain = 30;
    var alertTimer:Timer!
    var popup:PopupDialog!
    
    
    func checkFirstLogin()
    {
        
        
        if UserDefaults.standard.object(forKey: "first_login") == nil  {
            UserDefaults.standard.setValue(true, forKey: "first_login")
            
            popup = PopupDialog.init(title: "ระบบล็อคอัตโนมัติ", message: "ระบบจะทำการล็อคการเปิดเบอร์ใหม่ภายใน 30 วินาที เพื่อป้องกันบุคคลอื่นแอบอ้าง นำบัตรประชาชนไปเปิดหมายเลขโทรศัพท์",image:UIImage.init(named: "ic_warning_width"))
              popup.buttonAlignment = .horizontal
            
            if #available(iOS 10.0, *) {
                self.countdownTime = Timer.scheduledTimer(withTimeInterval: 30, repeats: false, block: { (time) in
                    self.requestLockNumber(is_lock: true)
                    self.popup.dismiss()
                })
            } else {
                self.countdownTime = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(closeDialog), userInfo: nil, repeats: false)
            }
            
            popup.addButton(PopupDialogButton.init(title: "ไม่ล็อค", action: {
                 self.countdownTime.invalidate()
                self.alertTimer.invalidate()
                   self.popup.dismiss()
               
            }))
            popup.addButton(PopupDialogButton.init(title: "ล็อค", action: {
                 self.countdownTime.invalidate()
                self.alertTimer.invalidate()
                  self.performSegue(withIdentifier: "lock", sender: nil)
            }))
            
            self.present(popup, animated: true,completion: {
             self.alertTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updatRemainTime), userInfo: self.popup, repeats: true)
            })
            
           
        }else{
            
            let is_lock : Bool =  UserDefaults.standard.bool(forKey: "is_lock") 
            
            
            if(is_lock)
            {
//              let  popup = PopupDialog.init(title: "ล็อค", message: "สถานะเลขหมายของท่านคือ \"ล็อค\" \nหากท่านต้องการปลดล็อคให้ไปยังเมนูล็อค",image:UIImage.init(named: "dialog_lock_ic"))
//                  popup.buttonAlignment = .horizontal
////                popup.addButton(PopupDialogButton.init(title: "ไม่ล็อค", action: {
////                      self.performSegue(withIdentifier: "lock", sender: nil)
////                }))
//                popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
//                     popup.dismiss()
//                }))
//                
//                 self.present(popup, animated: true, completion: nil)
                
                
//                let customIcon = #imageLiteral(resourceName: "lock_ic")
//                let alertview = JSSAlertView().show(self,
//                                                    title: "ล็อค",
//                                                    text: "สถานะเลขหมายของท่านคือ \"ล็อค\" \nหากท่านต้องการปลดล็อคให้ไปยังเมนูล็อค" ,
//                                                    cancelButtonText:"ตกลง",
//                                                    iconImage: customIcon)
//                
//                
//                alertview.setTitleFont("Athiti-Medium")
//                alertview.setTextFont("Athiti-Medium")
//                alertview.setButtonFont("Athiti-Medium")
//                alertview.setTextTheme(.dark)
                
                
                // TODO Alert is lock use this
                
//                let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "lock_number_alert") as! StepAlertViewController
//                myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//                myAlert.messageText = "สถานะเลขหมายของท่านคือ \"ล็อค\" หากท่านต้องการปลดล็อคให้ไปยังเมนูล็อค"
//                myAlert.titleText = "ล็อค"
//                self.present(myAlert, animated: true, completion: nil)
//
                print("isLock Number อยู่แล้ว")
                
            }else{
                
                popup = PopupDialog.init(title: "ระบบล็อคอัตโนมัติ", message: "ระบบจะทำการล็อคการเปิดเบอร์ใหม่ภายใน 5 นาที เพื่อป้องกันบุคคลอื่นแอบอ้าง นำบัตรประชาชนไปเปิดหมายเลขโทรศัพท์",image:UIImage.init(named: "ic_warning_width"))
                popup.buttonAlignment = .horizontal
                
                if #available(iOS 10.0, *) {
                    self.countdownTime = Timer.scheduledTimer(withTimeInterval: 300, repeats: false, block: { (time) in
                        self.requestLockNumber(is_lock: true)
                        self.popup.dismiss()
                    })
                } else {
                    self.countdownTime = Timer.scheduledTimer(timeInterval: 300, target: self, selector: #selector(closeDialog), userInfo: nil, repeats: false)
                }
                
                popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
//                    self.countdownTime.invalidate()
//                    self.alertTimer.invalidate()
                    self.popup.dismiss()
                    
                }))
//                let vc = UIApplication.topViewController()
                self.present(popup, animated: true,completion: nil)
                
//                let  popup = PopupDialog.init(title: "ระวัง", message: "เลขหมายของท่าน \"ไม่ล็อค\" \nกรุณาล็อค เพื่อความปลอดภัย",image:UIImage.init(named: "dialog_warning_ic"))
//                  popup.buttonAlignment = .horizontal
//                popup.addButton(PopupDialogButton.init(title: "ข้าม", action: {
//                    popup.dismiss()
//                }))
//                popup.addButton(PopupDialogButton.init(title: "ไปล็อคเลขหมาย", action: {
//                    self.performSegue(withIdentifier: "lock", sender: nil)
//                }))
//
//                self.present(popup, animated: true, completion: nil)
//
//
//                let alertVC = PMAlertController(title: "ระวัง", description: "เลขหมายของท่าน \"ไม่ล็อค\" \nกรุณาล็อค เพื่อความปลอดภัย", image: #imageLiteral(resourceName: "dialog_warning_ic"), style: .alert)
//
//                alertVC.addAction(PMAlertAction(title: "ข้าม", style: .cancel, action: { () -> Void in
//                    alertVC.dismiss(animated: true, completion: nil)
//                }))
//
//                alertVC.addAction(PMAlertAction(title: "ไปล็อคเลขหมาย", style: .default, action: { () in
//                      self.performSegue(withIdentifier: "lock", sender: nil)
//                    alertVC.dismiss(animated: true, completion: nil)
//                }))
                
            }
           
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchLock(_ sender: Any) {
        
        if (profile != nil) {
            
            if (profile?["email_verify"] as? Bool)! {
                 self.performSegue(withIdentifier: "lock", sender: self)
            }else{
                self.checkProfile();
                
                
                
                let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "confir_email_new_alert") as! StepAlertViewController
                myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                myAlert.titleText = "กรุณายืนยันอีเมล"
                myAlert.touchPositive  {
                    
                    self.performSegue(withIdentifier: "profile", sender: nil)
                   
                }
                myAlert.touchNagative {
                     self.performSegue(withIdentifier: "lock", sender: self)
                }
                myAlert.messageText = "เพื่อประโยชน์ของท่านในการรับ และจัดเก็บข้อมูลการใช้งานแอพฯ 3 ชั้น"
                 self.present(myAlert, animated: true, completion: nil)
//                let customIcon = #imageLiteral(resourceName: "confirm_email_ic")
//                let alertview = JSSAlertView().show(self,
//                                                    title: "กรุณายืนยันอีเมล",
//                                                    text: "เพื่อการใช้งานส่วนนี้ ท่านต้องยืนยันอีเมลที่หน้าข้อมูลผู้รักษาสิทธิ" ,
//                                                    buttonText:"ยืนยันอีเมล",
//                                                    cancelButtonText:"ยกเลิก",
//                                                    
//                                                    iconImage: customIcon)
//                
//                
//                alertview.setTitleFont("Athiti-Medium")
//                alertview.setTextFont("Athiti-Medium")
//                alertview.setButtonFont("Athiti-Medium")
//                alertview.setTextTheme(.dark)
//                alertview.addAction({
//                    self.performSegue(withIdentifier: "profile", sender: nil)
//                })
            }
            
           
        }else{

            
            let alertview = JSSAlertView().show(self,
                                                title: "รอสักครู่",
                                                text: "ระบบกำลังอัพเดทข้อมูลเป็นปัจจุบัน" ,
                                                buttonText:"ปิด"
                                                )
            
            
            alertview.setTitleFont("Athiti-Medium")
            alertview.setTextFont("Athiti-Medium")
            alertview.setButtonFont("Athiti-Medium")
            alertview.setTextTheme(.dark)
           

        }
        
    }
  
    @IBAction func touchSetting(_ sender: Any) {
        if (profile != nil) {
            
           if (profile?["email_verify"] as? Bool)! {
                self.performSegue(withIdentifier: "setting", sender: self)
            }else{
            
             self.checkProfile();
            
//                let  popup = PopupDialog.init(title: "กรุณายืนยันอีเมล", message: "เพื่อการใช้งานส่วนนี้ ท่านต้องยืนยันอีเมลที่หน้าข้อมูลผู้รักษาสิทธิ")
//                
//                popup.buttonAlignment  = .horizontal
//                popup.addButton(PopupDialogButton.init(title: "ยกเลิก", action: {
//                    popup.dismiss()
//                }))
//                popup.addButton(PopupDialogButton.init(title: "ยืนยันอีเมล", action: {
//                    self.performSegue(withIdentifier: "profile", sender: nil)
//                }))
//                
//                
//                self.present(popup, animated: true, completion: nil)
            
            let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "confir_email_new_alert") as! StepAlertViewController
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
             myAlert.titleText = "กรุณายืนยันอีเมล"
            myAlert.touchPositive  {
                
                self.performSegue(withIdentifier: "profile", sender: nil)
                
            }
            myAlert.touchNagative {
                self.performSegue(withIdentifier: "setting", sender: self)
            }
            myAlert.messageText = "เพื่อประโยชน์ของท่านในการรับ และจัดเก็บข้อมูลการใช้งานแอพฯ 3 ชั้น"
            self.present(myAlert, animated: true, completion: nil)
            
//            let customIcon = #imageLiteral(resourceName: "confirm_email_ic")
//            let alertview = JSSAlertView().show(self,
//                                                title: "กรุณายืนยันอีเมล",
//                                                text: "เพื่อการใช้งานส่วนนี้ ท่านต้องยืนยันอีเมลที่หน้าข้อมูลผู้รักษาสิทธิ" ,
//                                                buttonText:"ยืนยันอีเมล",
//                                                cancelButtonText:"ยกเลิก",
//                                                
//                                                iconImage: customIcon)
//            
//            
//            alertview.setTitleFont("Athiti-Medium")
//            alertview.setTextFont("Athiti-Medium")
//            alertview.setButtonFont("Athiti-Medium")
//            alertview.setTextTheme(.dark)
//            alertview.addAction({
//                self.performSegue(withIdentifier: "profile", sender: nil)
//            })
            
            
            }
            
            
        }else{
//            let  popup = PopupDialog.init(title: "รอสักครู่", message: "ระบบกำลังอัพเดทข้อมูลเป็นปัจจุบัน")
//              popup.buttonAlignment = .horizontal
//            popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
//                popup.dismiss()
//            }))
//            
//            
//            self.present(popup, animated: true, completion: nil)
            
            let alertview = JSSAlertView().show(self,
                                                title: "รอสักครู่",
                                                text: "ระบบกำลังอัพเดทข้อมูลเป็นปัจจุบัน" ,
                                                buttonText:"ปิด"
                                                )
            
            
            alertview.setTitleFont("Athiti-Medium")
            alertview.setTextFont("Athiti-Medium")
            alertview.setButtonFont("Athiti-Medium")
            alertview.setTextTheme(.dark)
            
        }
    }

    @IBAction func touchReport(_ sender: Any) {
        if (profile != nil) {
            
            if (profile?["email_verify"] as? Bool)! {
               self.performSegue(withIdentifier: "callcenter", sender: self)
            }else{
                
                let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "confir_email_new_alert") as! StepAlertViewController
                myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                 myAlert.titleText = "กรุณายืนยันอีเมล"
                myAlert.touchPositive  {
                    
                    self.performSegue(withIdentifier: "profile", sender: nil)
                    
                }
                myAlert.touchNagative {
                    self.performSegue(withIdentifier: "callcenter", sender: self)
                }
                myAlert.messageText = "เพื่อประโยชน์ของท่านในการรับ และจัดเก็บข้อมูลการใช้งานแอพฯ 3ชั้น"
                self.present(myAlert, animated: true, completion: nil)
            }
        }
        
    }
    @IBAction func touchCheck(_ sender: Any) {
        self.performSegue(withIdentifier: "check", sender: self)
    }
    @IBAction func touchProfile(_ sender: Any) { self.performSegue(withIdentifier: "profile", sender: self)

    }

}
