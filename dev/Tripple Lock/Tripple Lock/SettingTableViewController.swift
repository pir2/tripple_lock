//
//  SettingTableViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 6/1/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import PopupDialog

import JSSAlertView

class SettingTableViewController: UITableViewController ,PasscodeDelegate {

    
    @IBAction func onChangeNoti(_ sender: Any) {
        
        
         UserDefaults.standard.set(noti_switch.isOn,forKey: "noti_status")
       
        
        
    }
    @IBOutlet weak var noti_switch: UISwitch!
    func failedPassCode() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func successPassCode(code : String) {
        
        self.dismiss(animated: true, completion: {
            let customIcon = #imageLiteral(resourceName: "complete_ic")
            let alertview = JSSAlertView().show(self,
                                                title: "เปลี่ยนรหัส",
                                                text: "สำเร็จ",
                                                buttonText: "ปิด",
//                                                color: UIColorFromHex(0x9b59b6, alpha: 1),
                                                iconImage: customIcon)
            
            
            alertview.setTitleFont("Athiti-Medium")
            alertview.setTextFont("Athiti-Medium")
            alertview.setButtonFont("Athiti-Medium")
            alertview.setTextTheme(.dark)
            alertview.addAction {
                
             let app =    UIApplication.shared.delegate as! AppDelegate
                app.wantToLogin = true;
                self.dismiss(animated: true, completion: {
                    
                    UserDefaults.standard.removeObject(forKey: "token")
                    UserDefaults.standard.synchronize();
                    
                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "welcome")
                    UIApplication.shared.keyWindow?.rootViewController = viewController
                    
                })
            }
            
            
//            dialogAppearance.backgroundColor      = UIColor.white
//            dialogAppearance.titleFont            = UIFont.init(name: "Athiti-Medium", size: 30.0)!
//            dialogAppearance.titleColor           = UIColor(rgb: 0x222527)
//            dialogAppearance.titleTextAlignment   = .center
//            dialogAppearance.messageFont          = UIFont.init(name: "Athiti-Medium", size: 20.0)!
//            dialogAppearance.messageColor         = UIColor(rgb: 0x222527)
//            dialogAppearance.messageTextAlignment = .center
        })
//        let popup = PopupDialog.init(title: "เปลี่ยนรหัส", message: "สำเร็จ")
//        
//        popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
//            
//            popup.dismiss()
//        }))
        
//        self.present(popup, animated: true, completion: nil)
        
       
        
       
        
       
    }
    
    
    
    func recheckOldPasscode(code: String) -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (UIApplication.shared.delegate as! AppDelegate).currentView = self ;
         tableView.tableFooterView = UIView()

        let noti_status = UserDefaults.standard.bool(forKey: "noti_status")
         noti_switch.setOn(noti_status, animated: false);
       
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("select row %d",indexPath.row)
        
        switch indexPath.row {
        case 2:
//            Edit Email
             self.performSegue(withIdentifier: "editemail", sender: nil)
            break
        case 3:
//            new password
             self.performSegue(withIdentifier: "newpassword", sender: nil)
              break
        case 5:
//            about
            self.performSegue(withIdentifier: "about", sender: nil)

            break
        case 6 :
//            how to
             self.performSegue(withIdentifier: "howto", sender: nil)
            break;
        default:
            print("Default value")
        }
        
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination;
        
        if(vc is PasscodeViewController)
        {
            (vc as! PasscodeViewController).delegate = self
            (vc as! PasscodeViewController).askOldPass = true
            (vc as! PasscodeViewController).isDubbleAuth = true
            (vc as! PasscodeViewController).authStep = -1
            (vc as! PasscodeViewController).isSetNewAuth = false
        }
    }

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
