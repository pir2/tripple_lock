//
//  CallCenterRowTableViewCell.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 6/2/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit

import Kingfisher

class CallCenterRowTableViewCell: UITableViewCell {

    @IBOutlet weak var phoneNO: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var cover: UIImageView!
    var data:NSDictionary!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data : NSDictionary!)
    {
        self.data = data
        phoneNO?.text = data["tel"] as? String
        title?.text = data["provider_name"] as? String

//        let url =  URL(fileURLWithPath: data["image_url"] as! String)
        let url = URL(string:data?["image_url"] as! String)
        
        cover?.kf.setImage(with: url)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
