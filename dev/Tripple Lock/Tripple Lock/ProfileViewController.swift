//
//  ProfileViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/20/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit

import Alamofire
import RxSwift
import RxAlamofire
import PopupDialog
import JSSAlertView
import KVLoading


class ProfileViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var provinceTextfield: UITextField!
    @IBOutlet weak var distrtricTextfield: UITextField!
    @IBOutlet weak var postTextfield: UITextField!
    @IBOutlet weak var streeTextfield: UITextField!
    @IBOutlet weak var soiTextField: UITextField!
    @IBOutlet weak var addressTextfield: UITextField!
    @IBOutlet weak var buildingTextfield: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var personalIDTextfield: UITextField!
    @IBOutlet weak var surnameTextfield: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var subDistrict: UITextField!
    
    @IBOutlet weak var saveBTN: UIButton!
    let disposeBag = DisposeBag()
    var preventEmail:String = ""
    var preventData:NSDictionary!

    
    @IBAction func onTextChange(_ sender: Any) {
        
          if(!checkDataChange())
        {
            saveBTN.isHidden = true
        }else{
            saveBTN.isHidden = false
        }

    }
    @IBAction func touchBabck(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if(!checkDataChange())
        {
            saveBTN.isHidden = true
        }else{
            saveBTN.isHidden = false
        }
        
        return true
    }
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
          if( !checkDataChange())
          {
            saveBTN.isHidden = true
          }else{
            saveBTN.isHidden = false
        }
        
        return true
    }
    
    @IBAction func touchSave(_ sender: Any) {
        
//        if( self.emailTextField.text == preventEmail)
//        {
//            let popup = PopupDialog.init(title: "", message: "")
//            popup.buttonAlignment = .horizontal;
//            popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
//                popup.dismiss()
//            }))
//            
//            self.present(popup, animated: true, completion: nil)
//        }else
        
            if(self.isValidEmail(testStr: self.emailTextField.text!))
        {
           self.updateProfile()
        }else{
            let popup = PopupDialog.init(title: "แก้ไขข้อมูลส่วนตัว", message: "กรุณาใส่ อีเมลให้ถูกต้อง")
            popup.buttonAlignment = .horizontal;
            popup.addButton(PopupDialogButton.init(title: "ปิด", action: { 
                popup.dismiss()
            }))
            
            self.present(popup, animated: true, completion: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        (UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        addBottomLineToTextField(textField: provinceTextfield)
        addBottomLineToTextField(textField: distrtricTextfield)
        addBottomLineToTextField(textField: postTextfield)
        addBottomLineToTextField(textField: streeTextfield)
        addBottomLineToTextField(textField: soiTextField)
        addBottomLineToTextField(textField: addressTextfield)
        addBottomLineToTextField(textField: buildingTextfield)
        addBottomLineToTextField(textField: emailTextField)
        addBottomLineToTextField(textField: personalIDTextfield)
        addBottomLineToTextField(textField: nameTextField)
        addBottomLineToTextField(textField: surnameTextfield)
        addBottomLineToTextField(textField: subDistrict)
        
        requestProfile()
    }
    
    func requestProfile()
    {
        KVLoading.show()
        
        let token  = UserDefaults.standard.string(forKey: "token")
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"get_profile","access_token":token ?? "","device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"]! as? String
                    if(status == "ok")
                    {
                        self?.assignData(data: dict["result"]!["data"] as! NSDictionary)
                    }else{
                        
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
                        
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "",
                                                            text:  message ,
                                                            buttonText:"ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.addAction {
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        
                    }
                    
                  
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)

    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    
    func updateProfile()
    {
        
        KVLoading.show()
        
        let token:String = UserDefaults.standard.string(forKey: "token")!
        
        RxAlamofire.requestJSON(.post, Config.server_url,
                                parameters: [
                                    "action":"update_profile",
                                    "access_token":token,
                                    "first_name":self.nameTextField.text!,
                                    "last_name":self.surnameTextfield.text!,
                                    "email":self.emailTextField.text!,
                                    "address":self.addressTextfield.text!,
                                    "build":self.buildingTextfield.text!,
                                    "lane":self.soiTextField.text!,
                                    "road":self.streeTextfield.text!,
                                    "postcode":self.postTextfield.text!,
                                    "district":self.distrtricTextfield.text!,
                                    "sub_district":self.subDistrict.text!,
                                    "province":self.provinceTextfield.text!,
                                    "device_token":UIDevice.current.identifierForVendor!.uuidString
                                    
            ] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                let app = UIApplication.shared.delegate as! AppDelegate
                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                    
                    let status:String = (dict["result"]!["status"] as? String)!;
                    
                    if(status != "ok")
                    {
                        let errorObj:NSDictionary = dict["result"]!["error"] as! NSDictionary
//                        let alertController = UIAlertController(title: "เกิดข้อผิดพลาด", message: errorObj["message"] as? String, preferredStyle: .alert)
//                        
//                        let defaultAction = UIAlertAction(title: "ปิด", style: .default, handler: nil)
//                        alertController.addAction(defaultAction)
//                        
//                        self?.present(alertController, animated: true, completion: nil)
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "เกิดข้อผิดพลาด",
                                                            text: errorObj["message"] as? String ,
                                                            buttonText:"ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)

                    }else{
                     let resultObj:NSDictionary = dict["result"]! as! NSDictionary
//                    let alertController = UIAlertController(title: "สำเร็จ", message: (resultObj["message"] as? String)?.html2String, preferredStyle: .alert)
//                    
//                    let defaultAction = UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
//                        self?.dismiss(animated: true, completion: nil)
//                    })
//                   
//                    alertController.addAction(defaultAction)
//                    
//                    self?.present(alertController, animated: true, completion: nil)
                        
                        let customIcon = #imageLiteral(resourceName: "complete_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "สำเร็จ",
                                                            text:  (resultObj["message"] as? String)?.html2String ,
                                                            buttonText:"ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        alertview.addAction({
//                             self?.dismiss(animated: true, completion: nil)
                            self?.navigationController?.popViewController(animated: true)
                        })
                        
                    }
                    
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)

    }
    
    func checkDataChange()->Bool
    {
        var changed = false
        
        if(!(self.addressTextfield.text == preventData["address"] as? String))
        {
            changed = true
        }else if(!(self.buildingTextfield.text == preventData["build"] as? String)){
            changed = true
        }else if(!(self.emailTextField.text == preventData["email"] as? String)){
            changed = true
        }else if(!(self.nameTextField.text == preventData["first_name"] as? String)){
            changed = true
        }else if(!(self.surnameTextfield.text == preventData["last_name"] as? String)){
            changed = true
        }else if(!(self.streeTextfield.text == preventData["road"] as? String)){
            changed = true
        }else if(!(self.soiTextField.text == preventData["lane"] as? String)){
            changed = true
        }else if(!(self.postTextfield.text == preventData["post_code"] as? String)){
            changed = true
        }else if(!(self.distrtricTextfield.text == preventData["district"] as? String)){
            changed = true
        }else if(!(self.provinceTextfield.text == preventData["province"] as? String)){
            changed = true
        }else if(!(self.subDistrict.text == preventData["sub_district"] as? String)){
            changed = true
        }




    
        return changed
    }
    
    func assignData(data:NSDictionary)
    {
        
        preventData = data
        
        self.addressTextfield.text = data["address"] as? String
        self.buildingTextfield.text = data["build"] as? String
        self.emailTextField.text = data["email"] as? String
        self.nameTextField.text = data["first_name"] as? String
        self.surnameTextfield.text = data["last_name"] as? String
        
        var personalID = data["id"] as? String
       
        
        let start = personalID?.index((personalID?.startIndex)!, offsetBy: 7);
        let end = personalID?.index((personalID?.startIndex)!, offsetBy: 7 + 5);
        personalID?.replaceSubrange(start!..<end!, with: "XXXXX")
        
        self.personalIDTextfield.text = personalID
        
        self.streeTextfield.text = data["road"] as? String
        self.soiTextField.text = data["lane"] as? String
        self.postTextfield.text = data["post_code"] as? String
        self.distrtricTextfield.text = data["district"] as? String
        self.provinceTextfield.text = data["province"] as? String
        self.subDistrict.text = data["sub_district"] as? String
        
        preventEmail = (data["email"] as? String)!

        if( self.emailTextField.text == preventEmail)
        {
            saveBTN.isHidden = true
        }else{
            saveBTN.isHidden = false
        }

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func addBottomLineToTextField(textField : UITextField) {
        let border = CALayer()
        let borderWidth = CGFloat(0.8)
        border.borderColor = UIColor.init(colorLiteralRed: 29/255, green: 29/255, blue: 38/255, alpha: 0.6).cgColor
        border.frame = CGRect(x:0, y:textField.frame.size.height - borderWidth,width: textField.frame.size.width, height:textField.frame.size.height)
        border.borderWidth = borderWidth
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



