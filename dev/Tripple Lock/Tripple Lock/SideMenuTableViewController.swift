//
//  SideMenuTableViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 5/17/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import SideMenu
import PopupDialog
import JSSAlertView
class SideMenuTableViewController: UITableViewController {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var recentLabel: UILabel!
    @IBAction func tappedBack(_ sender: Any) {
//        dismiss(animated: true, completion: nil);
        self.sideMenuController?.toggle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        let nameString = UserDefaults.standard.string(forKey: "username") as? String ?? " - "
        usernameLabel.text = nameString
        
//        recentLabel.text = "ใช้งานล่าสุด " + UserDefaults.standard.string(forKey: "last_login")!
        
        displayLastLogin()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        displayLastLogin()
    }
    
    func displayLastLogin()
    {
        var last_login = UserDefaults.standard.string(forKey: "last_login") as? String ??  " - "
        
        if(last_login == " - ")
        {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateFormatter.locale = Locale.init(identifier: "th_TH");
            UserDefaults.standard.setValue(dateFormatter.string(from: Date.init()), forKey: "last_login")
            UserDefaults.standard.synchronize();
            
            last_login = dateFormatter.string(from: Date.init())
            
        }
        
        do
        {
            recentLabel.text = "ใช้งานล่าสุด " + last_login
            
            
        }catch is Error {
            
            
            
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.sideMenuController?.toggle()
        switch indexPath.row {
        case 0:
//            self.dismiss(animated: true, completion: nil)
            
            break
        case 1:
         ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "check", sender: nil)
            break
        case 2:
            let profile =  (UIApplication.shared.delegate as! AppDelegate).profileData
            if (profile?["email_verify"] as? Bool)! {
                ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "lock", sender: nil)
            }else{
                let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "confir_email_new_alert") as! StepAlertViewController
                myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                 myAlert.titleText = "กรุณายืนยันอีเมล"
                myAlert.touchPositive  {
                    
                    ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "profile", sender: nil)     
                    
                }
                myAlert.touchNagative {
                   ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "lock", sender: nil)
                }
                myAlert.messageText = "เพื่อประโยชน์ของท่านในการรับ และจัดเก็บข้อมูลการใช้งานแอพฯ 3ชั้น"
                self.present(myAlert, animated: true, completion: nil)
            }
            break
        case 3:
            ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "profile", sender: nil)
            break
        case 4:
            ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "history", sender: nil)
            break
        case 5:
          
            
            let profile =  (UIApplication.shared.delegate as! AppDelegate).profileData
            if (profile?["email_verify"] as? Bool)! {
                ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "callcenter", sender: nil)
            }else{
                
                let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "confir_email_new_alert") as! StepAlertViewController
                myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                 myAlert.titleText = "กรุณายืนยันอีเมล"
                myAlert.touchPositive  {
                    
                    ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "profile", sender: nil)
                }
                myAlert.touchNagative {
                    ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "callcenter", sender: nil)
                }
                myAlert.messageText = "เพื่อประโยชน์ของท่านในการรับ และจัดเก็บข้อมูลการใช้งานแอพฯ 3ชั้น"
                self.present(myAlert, animated: true, completion: nil)
                
            }

            
            
            break
        case 6:
            let profile =  (UIApplication.shared.delegate as! AppDelegate).profileData
            if (profile?["email_verify"] as? Bool)! {
                ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "setting", sender: nil)
            }else{
                
                let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "confir_email_new_alert") as! StepAlertViewController
                myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                 myAlert.titleText = "กรุณายืนยันอีเมล"
                myAlert.touchPositive  {
                    
                    ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "profile", sender: nil)
                    
                }
                myAlert.touchNagative {
                   ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "setting", sender: nil)
                }
                myAlert.messageText = "เพื่อประโยชน์ของท่านในการรับ และจัดเก็บข้อมูลการใช้งานแอพฯ 3ชั้น"
                self.present(myAlert, animated: true, completion: nil)

            }
            break
        case 7:
            ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "suggestion", sender: nil)
            break
        case 8:
            ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "about", sender: nil)
            break
        case 9:
            ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier: "manual", sender: nil)
            break
        case 10:
            
//            let  popup = PopupDialog.init(title:"ออกจากระบบ", message:"คุณต้องการออกจากระบบใช่หรือไม่")
//            
//            popup.addButton(PopupDialogButton.init(title: "ไม่", action: {
//                popup.dismiss()
//            }))
//            popup.buttonAlignment = .horizontal
//            popup.addButton(PopupDialogButton.init(title: "ใช่", action: {
//                popup.dismiss()
//                
//                UserDefaults.standard.removeObject(forKey: "token")
//                UserDefaults.standard.synchronize();
//                
//                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "welcome")
//                UIApplication.shared.keyWindow?.rootViewController = viewController
//            }))
//            
//            present(popup, animated: true, completion: nil)
            
            
            let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
            let alertview = JSSAlertView().show(self,
                                                title: "ออกจากระบบ",
                                                text: "คุณต้องการออกจากระบบใช่หรือไม่" ,
                                                buttonText: "ใช่",
                                                cancelButtonText:"ไม่",
                                                iconImage: customIcon)
            
            
            alertview.setTitleFont("Athiti-Medium")
            alertview.setTextFont("Athiti-Medium")
            alertview.setButtonFont("Athiti-Medium")
            alertview.setTextTheme(.dark)
            alertview.addAction({
                
                UserDefaults.standard.removeObject(forKey: "token")
                UserDefaults.standard.synchronize();
                
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "welcome")
                UIApplication.shared.keyWindow?.rootViewController = viewController
                
            })
            alertview.addCancelAction({
                
            })

            
            
            
            
//            let is_lock  = UserDefaults.standard.bool(forKey: "is_lock")
//            if(is_lock)
//            {
//                let  popup = PopupDialog.init(title:"ล็อค", message:"สถานะรหัสบัตรประชาชนของท่านคือ ล็อค กรุณาปลดล็อค ก่อนยกเลิกการใช้แอพ")
//                
//                popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
//                    popup.dismiss()
//                }))
//                
//                present(popup, animated: true, completion: nil)
//            }else{
//                
//                
//                let  popup = PopupDialog.init(title:"ยกเลิกการใช้แอพ", message:"เมื่อกดยกเลิกแล้วจะไม่สามารถเข้าใช้งานได้อีก")
//                
//                popup.addButton(PopupDialogButton.init(title: "ยกเลิก", action: {
//                    popup.dismiss()
//                }))
//                popup.buttonAlignment = .horizontal
//                popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
//                    popup.dismiss()
//                    
//                    UserDefaults.standard.removeObject(forKey: "token")
//                    UserDefaults.standard.synchronize();
//                    
//                     ( UIApplication.shared.delegate as! AppDelegate).homeView?.performSegue(withIdentifier:"welcome", sender: self)
//                }))
//                
//                present(popup, animated: true, completion: nil)
//                
//                
//             
//            }
            break
        default:
            break
        }
        
        self.tableView.deselectRow(at: indexPath, animated: false)
    }

    // MARK: - Table view data source

   
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
