//
//  OperatorMobileListTableViewCell.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 5/29/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit

class OperatorMobileListTableViewCell: UITableViewCell {

    @IBOutlet weak var reportStack: UIStackView!
    @IBOutlet weak var mainPhoneNOLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var useStatusLabel: UILabel!
    @IBOutlet weak var confirmLabel: UILabel!
    @IBOutlet weak var confirmImage: UIImageView!
    @IBOutlet weak var useImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func initData(data: NSDictionary)
    {
        
        var mobile_no = data["mobile_no"] as? String
         let start = mobile_no?.index((mobile_no?.startIndex)!, offsetBy: 2);
        let end = mobile_no?.index((mobile_no?.startIndex)!, offsetBy: 2 + 4);
//        mobile_no?.replaceSubrange(start!..<end!, with: "-XXXX-")
        
        mobileNumberLabel.text  = mobile_no
        
        if(data["is_main_no"] as! Bool)
        {
            mainPhoneNOLabel.isHidden = false
        }
        
        if(data["is_suspicious"] as! Bool )
        {
            reportStack.isHidden = false
            confirmImage.isHidden = true
            confirmLabel.isHidden = true
        }else{
            reportStack.isHidden = true
            confirmImage.isHidden = false
            confirmLabel.isHidden = false
        }
        
        if(data["active"] as! Bool)
        {
            useStatusLabel.text = "ใช้งาน"
        }else{
            useStatusLabel.text = "ไม่ระบุ"
        }
        
        if(data["verified"] as! Bool)
        {
            
            confirmLabel.text = "ยืนยัน"
        }else{
            confirmLabel.text = "ไม่ยืนยัน"
        }
        
        confirmLabel.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
