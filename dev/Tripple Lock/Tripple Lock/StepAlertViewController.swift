//
//  StepAlertViewController.swift
//  3Steps
//
//  Created by PiR2 Developer MacPro on 8/21/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//



import UIKit

protocol StepAlertDelegate
{
    func dismiss()
    func positiveTap()
    func nagativeTap()
}

class StepAlertViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var submessageLabel: UILabel!
    
    var delegate :StepAlertDelegate? = nil
    
    var titleText:String! = ""
    var messageText:String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.titleLabel.text = titleText
        self.messageLabel.text = messageText
        // Do any additional setup after loading the view.
    }
    
    typealias CallBack =  () -> Void
    
    var onTouchPositivee:CallBack!
    var onTouchNagative:CallBack!
    var onTouchDismissCallback:CallBack!

    
    func touchNagative(event:@escaping CallBack)
    {
        self.onTouchNagative = event
    }
    
    func touchPositive(event:@escaping CallBack)
    {
        self.onTouchPositivee = event
    }
    
    func touchDismissCallback(event:@escaping CallBack)
    {
        self.onTouchDismissCallback = event
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func tapClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTouchNagativeTap(_ sender:Any)
    {
        self.dismiss(animated: true) {
            if (self.delegate != nil) {
                self.delegate?.nagativeTap()
            }
            
            if(self.onTouchNagative != nil)
            {
                self.onTouchNagative()
            }
        }
    }
    
    @IBAction func onTouchPositive(_ sender:Any)
    {
        self.dismiss(animated: true) {
            if (self.delegate != nil) {
                self.delegate?.positiveTap()
            }
            
            if(self.onTouchPositivee != nil)
            {
                self.onTouchPositivee()
            }
        }
    }

    @IBAction func onTouchDismiss(_ sender: Any) {
        self.dismiss(animated: true) { 
            if (self.delegate != nil) {
                self.delegate?.dismiss()
            }
            
            if(self.onTouchDismissCallback != nil)
            {
                self.onTouchDismissCallback()
            }
        }
    }
   


}
