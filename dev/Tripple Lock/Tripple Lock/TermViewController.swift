//
//  TermViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 6/15/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import PopupDialog
import JSSAlertView

class TermViewController: UIViewController {

    var userIsAccept:Bool = false
    
    
    @IBOutlet weak var bottom_constrant: NSLayoutConstraint!
    @IBOutlet weak var acceptBTN: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        let isAccept = UserDefaults.standard.bool(forKey: "accept_term")
        if(isAccept)
        {
            acceptBTN.isHidden  = true;
            term_checkbox.setImage(UIImage.init(named: "term_checked"), for: .normal);
            term_checkbox.isEnabled = false;
            
            bottom_constrant.constant = -54;
            
        }else{
            bottom_constrant.constant = 24;
            acceptBTN.isEnabled = false
            
            
           
            
                
            
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let isShowAlert = UserDefaults.standard.value(forKey: "first_alert")
        
        if(isShowAlert == nil)
        {
            
            
              let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
        let alertview = JSSAlertView().show(self, title: "",
                                            text: "กรุณาสมัครใช้ที่ศูนย์บริการค่ายมือถือครั้งแรกเเละครั้งเดียวเท่านั้น ก่อนลงทะเบียนเข้าใช้งาน  เพื่อปกป้องรักษาสิทธิของท่าน" ,
                                            buttonText: "ตกลง",
                                            iconImage:customIcon)
        
        
        alertview.setTitleFont("Athiti-Medium")
        alertview.setTextFont("Athiti-Medium")
        alertview.setButtonFont("Athiti-Medium")
        alertview.setTextTheme(.dark)
        }
    }

    @IBAction func onTouchTermCheckbox(_ sender: Any) {
        
        if(userIsAccept)
        {
            userIsAccept = false
            acceptBTN.isEnabled = false;
            
            term_checkbox.setImage(UIImage.init(named: "term_unchecked"), for: .normal);
        }else{
             userIsAccept = true
             acceptBTN.isEnabled = true;
            term_checkbox.setImage(UIImage.init(named: "term_checked"), for: .normal);
        }
        
    }
    
    @IBOutlet weak var term_checkbox: UIButton!
    
    
    
    @IBAction func onTouchOK(_ sender: Any) {
        
        if(userIsAccept)
        {
            UserDefaults.standard.set(true, forKey: "accept_term")
            UserDefaults.standard.synchronize();
            
            dismiss(animated: true, completion: nil);
            
        }else{
            let  popup = PopupDialog.init(title: "Terms of Use", message: "กรุณากดยอมรับข้อตกลงก่อนเข้าใช้งาน")
            
            popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
                popup.dismiss()
            }))
            
            self.present(popup, animated: true, completion: nil)

        }
    }
    
    @IBAction func onTouchBack(_ sender: Any) {
        dismiss(animated: true, completion: nil);
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
