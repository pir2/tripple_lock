//
//  CheckViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import RxAlamofire
import KVLoading

class CheckViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var tableview: UITableView!
     var operatorLists:NSMutableArray  = []
    
     let disposeBag = DisposeBag()
    
    var profileData : NSDictionary?
    var operatorDatas:NSMutableArray = []
    
    @IBOutlet weak var amountNOLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var personalIDLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        (UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        self.tableview.delegate = self;
        self.tableview.dataSource = self;
        
        self.operatorLists = NSMutableArray();
        
       
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.requestData()
        
        self.getProfile()
    }
    
    func updateProfileView()
    {
        var personalID = self.profileData?.value(forKey: "id") as? String
        let start = personalID?.index((personalID?.startIndex)!, offsetBy: 7);
        let end = personalID?.index((personalID?.startIndex)!, offsetBy: 7 + 5);
        personalID?.replaceSubrange(start!..<end!, with: "XXXXX")
        
        
        self.personalIDLabel.text = personalID
        self.usernameLabel.text = (self.profileData?.value(forKey: "first_name") as? String)! + " " +  (self.profileData?.value(forKey: "last_name") as? String)!
    }
    
    func getProfile()
    {
        
        KVLoading.show()
        
        let token  =   UserDefaults.standard.string(forKey: "token")
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"get_profile","access_token":token ?? "","device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                
                KVLoading.hide()
                
               
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                            
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }else{
                        
                        self?.profileData =  dict["result"]!["data"]! as! NSDictionary
                        
                        self?.updateProfileView()
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)

    }
    
    func requestData()
    {
        KVLoading.show()
        
        let token  =   UserDefaults.standard.string(forKey: "token")
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"check_list","access_token":token ?? "","device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }else{
                    
                    self?.operatorDatas =  NSMutableArray.init(array: dict["result"]!["data"]! as! NSArray)
                    self?.tableview.reloadData()
                        
                    
                    var totalNumber:Int = 0
                        for i : NSDictionary in (self?.operatorDatas as! NSArray as! [NSDictionary])
                    {
                        totalNumber += (i["list"] as! NSArray).count
                    }
                        
                    self?.amountNOLabel.text = String(totalNumber)
                    }
                                   }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchBack(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }

//    TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "operatorDetail", sender: operatorDatas[indexPath.row] as! Dictionary<String, AnyObject>)
    }
//    TAbleviewDataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54;
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 41;
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header:UITableViewCell = tableview.dequeueReusableCell(withIdentifier: "Header")! as UITableViewCell
        return header
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:OperatorTableViewCell  = tableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! OperatorTableViewCell
        
        cell.setData(data: operatorDatas[indexPath.row] as! Dictionary<String, AnyObject> )
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return operatorDatas.count;
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    let vc = segue.destination
        
        if (vc is OperatorDetaileViewController)
        {
            (vc as? OperatorDetaileViewController)?.data = sender as AnyObject
        }
    }
    

}
