//
//  OperatorDetaileViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/20/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import Kingfisher

class OperatorDetaileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,PhoneNoDelegate {

    @IBOutlet weak var tableView: UITableView!
    var numberList:NSMutableArray = []
    
    @IBOutlet weak var no_number: UIImageView!
    @IBOutlet weak var operatorImage: UIImageView!
    @IBOutlet weak var totalNumber: UILabel!
    var isReport:Bool  = false;
    var data: AnyObject?
    var lastIndex:Int = 0;
    
    @IBAction func reportLoseNumber(_ sender: Any) {
        self.performSegue(withIdentifier: "report_lose", sender: nil)
    }
    @IBAction func touchBack(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        (UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        self.tableView.dataSource = self;
        self.tableView.delegate = self;

       let url = URL(string:data?["image_url"] as! String)
        operatorImage.kf.setImage(with:url)
        
        let amountNumber:NSNumber = (data?["count"] as! NSNumber)
        
        totalNumber.text = amountNumber.stringValue
        
      
        
//        operatorImage.image = UIImage.ini
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func reportStatus(result: Bool) {
//       self.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }

//    UITableViewDelegate
    
    //    TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        lastIndex = indexPath.row;
        
        if(self.isReport)
        {
              self.performSegue(withIdentifier: "reportNo", sender: self)
        }else{
          self.performSegue(withIdentifier: "phoneNODetail", sender:  (data!["list"] as! NSArray)[indexPath.row] as! NSDictionary)
        }
    }
    //    TAbleviewDataSource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54;
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 41;
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Header")! as UITableViewCell
        return header
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:OperatorMobileListTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! OperatorMobileListTableViewCell
        cell.initData(data: (data!["list"] as! NSArray)[indexPath.row] as! NSDictionary)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        no_number.isHidden = (data!["list"] as! NSArray).count != 0
        
        let amountNumber:NSNumber = (data?["count"] as! NSNumber)
        
        
        if(amountNumber.intValue >= 50 )
        {
            no_number.image = #imageLiteral(resourceName: "message_for_over_50_number")
            no_number.isHidden = false
            no_number.contentMode = .top
            return 0;
        }else{
        
        return (data!["list"] as! NSArray).count;
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc : PhoneNODetailViewController  = segue.destination as! PhoneNODetailViewController
        if(sender != nil)
        {
        vc.NODetail = sender as? NSDictionary
        }
        vc.delegate = self
        vc.provider_id =  data?["provider_id"] as? String
        vc.operatorImageUrl = data?["image_url"] as? String
    }

}
