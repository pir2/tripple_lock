//
//  SettingViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire
import PopupDialog
import KVLoading

class SettingViewController: UIViewController {

    
    var noti:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
(UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        
       noti =  UserDefaults.standard.bool(forKey: "noti_status")
        // Do any additional setup after loading the view.
    }

    @IBAction func onTouchSave(_ sender: Any) {
        UserDefaults.standard.synchronize();
        
          self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchBack(_ sender: Any) {
        
        UserDefaults.standard.set(noti,forKey: "noti_status");
        
        UserDefaults.standard.synchronize();
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    let disposeBag  = DisposeBag()
    
    func requestTerminateApp()  {
        
        KVLoading.show()
        
        let pernonalID : String  = (UserDefaults.standard.string(forKey: "personalID"))!
        let passcode : String  = (UserDefaults.standard.string(forKey: "passcode"))!
        let mobileNumber : String  = (UserDefaults.standard.string(forKey: "mobile"))!
        
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"disable_user","id":pernonalID ,"passcode":passcode,"mobile_no":mobileNumber,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
               
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                            
                             let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }else{
                        UserDefaults.standard.removeObject(forKey: "token")
                        UserDefaults.standard.synchronize();
                        
                        
                        let viewController = self?.storyboard?.instantiateViewController(withIdentifier: "welcome")
                        UIApplication.shared.keyWindow?.rootViewController = viewController
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
    }

    @IBAction func touchLogout(_ sender: Any) {
        
        let is_lock  = UserDefaults.standard.bool(forKey: "is_lock")
        if(is_lock)
        {
            let  popup = PopupDialog.init(title:"ล็อค", message:"สถานะรหัสบัตรประชาชนของท่านคือ ล็อค กรุณาปลดล็อค ก่อนยกเลิกการใช้แอพ")
            
            popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
                popup.dismiss()
            }))
            
            present(popup, animated: true, completion: nil)
        }else{
            
            let  popup = PopupDialog.init(title:"ยกเลิกการใช้แอพ", message:"เมื่อกดยกเลิกแล้วจะไม่สามารถเข้าใช้งานได้อีก")
            
            popup.addButton(PopupDialogButton.init(title: "ยกเลิก", action: {
                popup.dismiss()
            }))
            
            popup.buttonAlignment = .horizontal
            popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
                popup.dismiss()
                
                self.requestTerminateApp()
            }))
            
            present(popup, animated: true, completion: nil)
            
        }
        
        
//
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
