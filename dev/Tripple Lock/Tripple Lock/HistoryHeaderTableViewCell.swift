//
//  HistoryHeaderTableViewCell.swift
//  3Steps
//
//  Created by PiR2 Developer MacPro on 13/11/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit

class HistoryHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var headerText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
