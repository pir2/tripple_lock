//
//  MockUpViewController.swift
//  3Steps
//
//  Created by PiR2 Developer MacPro on 8/22/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import SideMenuController
import UIKit

class MockUpViewController: SideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()

        performSegue(withIdentifier: "embedInitialCenterController", sender: nil)
        performSegue(withIdentifier: "embedSideController", sender: nil)
        // Do any additional setup after loading the view.
    }

  

}
