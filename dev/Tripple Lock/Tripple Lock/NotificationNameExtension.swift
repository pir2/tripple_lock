//
//  NotificationNameExtension.swift
//  3Steps
//
//  Created by Bank on 16/10/2561 BE.
//  Copyright © 2561 PiR2 Developer MacPro. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let lockStatus = Notification.Name(rawValue: "lockStatus")
}
