//
//  RegisterViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import RxAlamofire
import PopupDialog
import JSSAlertView
import KVLoading

class RegisterViewController: UIViewController,ReceiveOTPDelegate,PasscodeDelegate,UITextFieldDelegate {
    func recheckOldPasscode(code: String) -> Bool {
        return true
    }


    let disposeBag  = DisposeBag()
    @IBOutlet weak var phone_no_tf: UITextField!
    @IBOutlet weak var personal_no_tf: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
(UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validateField() -> Bool {
        var result  = true;
        
        if((self.phone_no_tf.text?.characters.count)! < 10)
        {
            result = false
            let popup = PopupDialog.init(title: "ลงทะเบียนไม่สำเร็จ", message: "กรุณากรอกเบอร์โทรศัพท์\nให้ครบ 10 หลัก")
            popup.addButton(PopupDialogButton.init(title: "ตกลง", action: { 
                popup.dismiss()
            }))
            self.present(popup, animated: true, completion: nil)
        }else  if((self.personal_no_tf.text?.characters.count)! < 13)
        {
            result = false
            let popup = PopupDialog.init(title: "ลงทะเบียนไม่สำเร็จ", message: "กรุณากรอกเลขบัตรประชาชน\nให้ครบ 13 หลัก")
            popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
                popup.dismiss()
            }))
            self.present(popup, animated: true, completion: nil)
        }
        
      
        return result;
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if(textField == phone_no_tf)
        {
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 10
        }else{
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 13
        }
    }
    
    func checkEmailAndPhone()
    {
        KVLoading.show()
        
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"check_email_id","id":self.personal_no_tf.text ?? "" ,"mobile_no":self.phone_no_tf.text ?? "","is_register":true,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                let app = UIApplication.shared.delegate as! AppDelegate
                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = (error["message"]  as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }else{
                       
                        UserDefaults.standard.setValue(self?.personal_no_tf.text, forKey: "personalID")
                        UserDefaults.standard.setValue(self?.phone_no_tf.text, forKey: "mobile")
                         UserDefaults.standard.setValue(self?.phone_no_tf.text, forKey: "phone_otp")
                        UserDefaults.standard.synchronize();
                        
                         self?.performSegue(withIdentifier: "otp", sender: dict["result"]!["data"]!)
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
    }
    
    func saveUser()
    {
        
    }
    
    @IBAction func touchRegister(_ sender: Any) {
        if(validateField())
        {
            
            self.checkEmailAndPhone();
//            self.performSegue(withIdentifier: "otp", sender: nil)
        }else{
            
        }
       
       // self.performSegue(withIdentifier: "home", sender: self)
    }

    @IBAction func touchBack(_ sender: Any) {
        self.dismiss(animated: true) { 
            
        }
    }
//    PasscodeDelegate
    func successPassCode(code : String) {
        self.dismiss(animated: false){
            UserDefaults.standard.set(code, forKey: "passcode")
            UserDefaults.standard.synchronize()
            
            KVLoading.show()
            
            RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"save_user","id":self.personal_no_tf.text ?? "" ,"mobile_no":self.phone_no_tf.text ?? "","passcode":code,"is_register":true,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
                .debug()
                .subscribe(onNext: { [weak self] (r, json) in
                    
                    let app = UIApplication.shared.delegate as! AppDelegate
                    app.onResponseValue.value = json
                    
                    if let dict = json as? [String: AnyObject] {
                        print(dict["result"]!["data"]!)
                        let status = dict["result"]!["status"] as! String
                        if(status == "error")
                        {
                              KVLoading.hide()
                            
                            let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                            let message:String = (error["message"]  as? String)!
                            
                            
                            let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                                alert.dismiss(animated: true, completion: nil);
                            }))
                            self?.present(alert, animated:true, completion: nil)
                        }else{
                            
                            UserDefaults.standard.set(code, forKey: "passcode")
                           

                            
                             UserDefaults.standard.removeObject(forKey: "first_login")
                            UserDefaults.standard.synchronize()
                            
                            self?.autoLogin(personalID: (self?.personal_no_tf.text!)!, mobile: (self?.phone_no_tf.text!)!, code: code)
                            
                            
                            
                            
                        }
                    }
                    }, onError: { [weak self] (error) in
                        
                })
                .addDisposableTo(self.disposeBag)
            
            
     
        }
        
    }
    
    func autoLogin(personalID:String,mobile:String,code:String)
    {
        let params = ["action":"authenticate","id":personalID ,"mobile_no":mobile ,"passcode":code,"device_token":UIDevice.current.identifierForVendor!.uuidString,"device_type":"ios","is_mobile":true] as [String : Any]
        
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: params )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                  KVLoading.hide()
                
//                let app = UIApplication.shared.delegate as! AppDelegate
//                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]! ?? "no data")
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = (error["message"]  as? String)!
                        //                            let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        //                            alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                        //                                alert.dismiss(animated: true, completion: nil);
                        //                            }))
                        //                            self?.present(alert, animated:true, completion: nil)
                        
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "",
                                                            text: "message" ,
                                                            buttonText:"ปิด",
                                                            iconImage: customIcon)
                        
                        alertview.addAction {
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        
                        
                        
                    }else{
                        
                        let result = dict["result"]!["data"]! as! NSDictionary
                        let access_token = result["access_token"]! as? String
                        
                        UserDefaults.standard.set(code, forKey: "passcode")
                        
                        
                        UserDefaults.standard.setValue(access_token, forKey: "token");
                        UserDefaults.standard.synchronize()
                        
                        
                        self?.performSegue(withIdentifier: "home", sender: Any?.self)
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(self.disposeBag)

    }
    func failedPassCode() {
        
    }
//    ReceiveOTPDelegate
    func successOTP() {
       self.dismiss(animated: false) { 
        self.performSegue(withIdentifier: "passcode", sender: nil)
        }
    }
    func failedOTP() {
        
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.destination is ReciveOTPViewController)
        {
            let vc = segue.destination as! ReciveOTPViewController
            
            vc.delegate = self
        }else if(segue.destination is PasscodeViewController){
            let vc = segue.destination as! PasscodeViewController
            vc.askOldPass = false
            vc.isRequireForget = false
            vc.isOneTimeMode = false
            vc.delegate = self
        }
    }
 
}
