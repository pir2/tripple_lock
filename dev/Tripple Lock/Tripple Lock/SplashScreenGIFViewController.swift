//
//  SplashScreenGIFViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 6/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import SwiftyGif

class SplashScreenGIFViewController: UIViewController,SwiftyGifDelegate {

    @IBOutlet var gitView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let gif = UIImage(gifName: "NBTC_SplashScreen.gif", levelOfIntegrity:0.5)
        gitView.delegate  = self
        gitView.setGifImage(gif, loopCount: 2);
        gitView.startAnimatingGif()
        
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { (action) in
                self.performSegue(withIdentifier: "home", sender: nil)
            })
        } else {
            Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.endTime), userInfo: nil, repeats: false)
        }
    }
    
    func endTime()
    {
        self.performSegue(withIdentifier: "home", sender: nil)
    }
    
    
    func gifDidStop() {
        self.performSegue(withIdentifier: "home", sender: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
