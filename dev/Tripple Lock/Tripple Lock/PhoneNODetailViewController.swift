//
//  PhoneNODetailViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/20/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

extension String{
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
}

import UIKit
import Kingfisher
import Alamofire
import RxSwift
import RxAlamofire
import PopupDialog
import JSSAlertView
import KVLoading



protocol PhoneNoDelegate {
   func reportStatus(result:Bool)
}

class PhoneNODetailViewController: UIViewController, PasscodeDelegate, UITextFieldDelegate {
    
  
    
    
    func failedPassCode() {
        
    }

    func recheckOldPasscode(code: String) -> Bool {
        return false
    }
    @IBOutlet weak var lose_phone_textfield: UITextField!
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }

            let newLength = text.count + string.count - range.length
            return newLength <= 10

    }
    
    func onConfirmLose(code:String)
    {
        KVLoading.show()
        
        let token:String = UserDefaults.standard.string(forKey: "token")!;
        let mobilephone:String = self.lose_phone_textfield.text!
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"check_lost_no","access_token":token,"mobile_no":mobilephone ,"provider_id":provider_id ?? ""] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                
                
                if let dict = json as? [String: AnyObject] {
                    
                    let status:String = dict["data"]!["status"] as! String
                   
                    
                    if(status == "ok")
                    {
                         let message:String = dict["data"]!["message"] as! String
                        let customIcon = #imageLiteral(resourceName: "complete_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "",
                                                            text: message.html2String,
                                                            buttonText: "ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        alertview.addAction({
                            
                            if((self?.delegate) != nil)
                            {
                                self?.delegate?.reportStatus(result: true)
                            }
                            
                            self?.navigationController?.popViewController(animated: true)
                            
                        })
                    }else{
                        
                        let message = dict["data"]!["message"] as! String
//                        let message = (errorObj["message"] as! NSDictionary)["message"] as! String
                        let popup = PopupDialog.init(title: "แจ้งเบอร์ขาด", message: message)
                        
                        
                        popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
                            popup.dismiss()
                            //                            self?.dismiss(animated: true, completion: nil)
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                            
                            self?.navigationController?.popViewController(animated: true)
                        }))
                        
                        self?.present(popup, animated: true, completion: nil)
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
//                                                            title: "ยกเลิกแจ้งเบอร์แปลกปลอม",
                                                            title: "แจ้งเบอร์ขาด",
                                                            text: message,
                                                            buttonText: "ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        alertview.addAction({
                            self?.navigationController?.popViewController(animated: true)
                            //                            self?.dismiss(animated: true)
                            //                            {
                            if((self?.delegate) != nil)
                            {
                                self?.delegate?.reportStatus(result: true)
                            }
                            //                            }
                            
                            
                            
                        })
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
    }
    
    @IBAction func confirmLose(_ sender: Any) {
        
        self.type = 2
        self.performSegue(withIdentifier: "passcode", sender: nil);
        
        
    }
    
    var delegate:PhoneNoDelegate?
    @IBOutlet weak var verifiredLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var registerDateLabel: UILabel!
    @IBOutlet weak var phonenoLabel: UILabel!
    @IBOutlet weak var registerbyLabel: UILabel!
    @IBOutlet weak var operatorImage: UIImageView!
    @IBOutlet weak var report_btn: UIButton!
    public var NODetail:NSDictionary?
    public var operatorImageUrl:String?
    public var provider_id:String?
    @IBAction func touchBack(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
     let disposeBag = DisposeBag()
    var type:Int = 0
    
    func requestData()
    {
        KVLoading.show()
        
        let token = UserDefaults.standard.string(forKey: "token");
        let mobile_no = self.NODetail?["mobile_no"] as! String
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"request_suspicious_no","access_token":token ?? "","provider_id":provider_id ?? "","is_mobile":"true","mobile_no":mobile_no,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                let app = UIApplication.shared.delegate as! AppDelegate
                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                   
                    
                   
//                    let status:String = dict["result"]!["status"] as! String
                    
                     if(dict["data"] != nil)
                    {
//                        let alertController = UIAlertController(title: "สำเร็จ", message: dict["data"]!["message"] as! String, preferredStyle: .alert)
//                       
//                    
//                        let defaultAction = UIAlertAction(title: "ปิด", style: .default, handler: { (action) in
//                             self?.dismiss(animated: true)
//                             {
//                                if((self?.delegate) != nil)
//                                {
//                                    self?.delegate?.reportStatus(result: true)
//                                }
//                            }
//                        })
//                        
//                        alertController.addAction(defaultAction)
//                        
//                        self?.present(alertController, animated: true, completion: nil)
                        
                        
                        let customIcon = #imageLiteral(resourceName: "complete_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "",
                                                            text: "ขอให้ท่านไปยืนยันที่ศูนย์บริการและดำเนินการตามเงื่อนไขของศูนย์บริการเพื่อให้การแจ้งเสร็จสิ้นสมบูรณ์",
                                                            buttonText: "ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        alertview.addAction({
                            if((self?.delegate) != nil)
                            {
                                self?.delegate?.reportStatus(result: true)
                            }
                            self?.navigationController?.popViewController(animated: true)
                            
//                            self?.dismiss(animated: true)
//                            {
//                                if((self?.delegate) != nil)
//                                {
//                                    self?.delegate?.reportStatus(result: true)
//                                }
//                            }
                        })

                    }else{
                        
                        let errorObj:NSDictionary = dict["result"]!["error"] as! NSDictionary
                        let message = (errorObj["message"] as! NSDictionary)["message"] as! String
//                        let popup = PopupDialog.init(title: "แจ้งเบอร์แปลกปลอม", message: message)
//                        
//                        popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
//                           
//                            popup.dismiss()
//                           
//                        }))
//                        
//                        self?.present(popup, animated: true, completion: nil)
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "แจ้งเบอร์แปลกปลอม",
                                                            text:message,
                                                            buttonText: "ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)

                    }
                    
                   
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
    }
    
    
    
    func requestCancelData()
    {
        KVLoading.show()
        
        let token:String = UserDefaults.standard.string(forKey: "token")!;
        let mobilephone = self.NODetail?["mobile_no"] as! String
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"cancel_suspicious_no","access_token":token,"mobile_no":mobilephone,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                
                
                if let dict = json as? [String: AnyObject] {
                    
                    let status:String = dict["result"]!["status"] as! String
                    
                    if(status == "ok")
                    {
                        
                        
//                        let popup = PopupDialog.init(title: "ยกเลิกแจ้งเบอร์แปลกปลอม", message: dict["result"]!["message"] as! String)
                        
                        let customIcon = #imageLiteral(resourceName: "complete_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "",
                                                            text: "ยกเลิกการแจ้งระงับเบอร์แปลกปลอมสำเร็จ",
                                                            buttonText: "ปิด",
                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        alertview.addAction({
                            
                            if((self?.delegate) != nil)
                            {
                                self?.delegate?.reportStatus(result: true)
                            }
                            
                            self?.navigationController?.popViewController(animated: true)
                            
//                            self?.dismiss(animated: true)
//                            {
//                                
//                            }
                        })
                        
                        
                        
//                         let popup = PopupDialog.init(title: "", message: "ขอให้ท่านไปยืนยันที่ศูนย์บริการและดำเนินการตามเงื่อนไขของศูนย์บริการเพื่อให้การแจ้งเสร็จสิ้นสมบูรณ์")
//                        
//                        
//                        popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
//                            popup.dismiss()
//                            self?.dismiss(animated: true)
//                            {
//                                if((self?.delegate) != nil)
//                                {
//                                    self?.delegate?.reportStatus(result: true)
//                                }
//                            }
//                        }))
//                        
//                        self?.present(popup, animated: true, completion: nil)
                        
                    }else{
                        
                        let errorObj:NSDictionary = dict["result"]!["error"] as! NSDictionary
                        let message = (errorObj["message"] as! NSDictionary)["message"] as! String
                        let popup = PopupDialog.init(title: "ยกเลิกแจ้งเบอร์แปลกปลอม", message: message)
                        
                        
                        popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
                            popup.dismiss()
//                            self?.dismiss(animated: true, completion: nil)
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                            
                            self?.navigationController?.popViewController(animated: true)
                        }))
                        
                        self?.present(popup, animated: true, completion: nil)
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "ยกเลิกแจ้งเบอร์แปลกปลอม",
                                                            text: message,
                                                            buttonText: "ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        alertview.addAction({
                             self?.navigationController?.popViewController(animated: true)
//                            self?.dismiss(animated: true)
//                            {
                                if((self?.delegate) != nil)
                                {
                                    self?.delegate?.reportStatus(result: true)
                                }
//                            }
                            
                           
                            
                        })

                        
                        
                    }
                    
                    
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
    }
    
    func successPassCode(code: String) {
        
        if(self.type == 0)
        {
             self.requestCancelData();
        }else if(self.type == 2){
            self.onConfirmLose(code:code)
        }else{
             self.requestData();
        }
    }
    
    @IBAction func reportSpam(_ sender: Any) {
        
        if(report_btn.title(for: .normal) == "ยกเลิกแจ้งเบอร์แปลกปลอม")
        {
            let popup = PopupDialog.init(title: "ยืนยันการแจ้งยกเลิก \nเบอร์แปลกปลอม", message: "กรุณายืนยันการดำเนินการ", image: UIImage.init(named: "alert_ic"), buttonAlignment: .horizontal, transitionStyle: .fadeIn, gestureDismissal: true, completion: {
                
            })
            popup.addButton(PopupDialogButton.init(title: "ยืนยัน", height: 60, dismissOnTap: true, action: {
                
                self.type = 0
                self.performSegue(withIdentifier: "passcode", sender: nil);
            }))
 
            popup.addButton(PopupDialogButton.init(title: "ยกเลิก", height: 60, dismissOnTap: true, action: {
                
            }))
            
            
            self.present(popup, animated: true, completion: nil)
            
            
        }else{
            
            let popup = PopupDialog.init(title: "ยืนยันการแจ้งระงับ \nเบอร์แปลกปลอม", message: "กรุณายืนยันการดำเนินการ", image: UIImage.init(named: "alert_ic"), buttonAlignment: .horizontal, transitionStyle: .fadeIn, gestureDismissal: true, completion: {
                
            })
            popup.addButton(PopupDialogButton.init(title: "ยืนยัน", height: 60, dismissOnTap: true, action: {
                
                self.type = 1
                self.performSegue(withIdentifier: "passcode", sender: nil);
            }))

            popup.addButton(PopupDialogButton.init(title: "ยกเลิก", height: 60, dismissOnTap: true, action: {
                
            }))
            
            
            self.present(popup, animated: true, completion: nil)
            
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        let url = URL(string:operatorImageUrl!)
        operatorImage.kf.setImage(with:url)
        
        var personalID = NODetail?["registration_by"] as? String
        personalID?.insert("-", at: (personalID?.index((personalID?.startIndex)!, offsetBy: 12))!)
        personalID?.insert("-", at: (personalID?.index((personalID?.startIndex)!, offsetBy: 9))!)
        personalID?.insert("-", at: (personalID?.index((personalID?.startIndex)!, offsetBy: 5))!)
        personalID?.insert("-", at: (personalID?.index((personalID?.startIndex)!, offsetBy: 1))!)

        var start = personalID?.index((personalID?.startIndex)!, offsetBy: 7);
        var end = personalID?.index((personalID?.startIndex)!, offsetBy: 7 + 6);
        personalID?.replaceSubrange(start!..<end!, with: "XXXXX-")
        
        
        registerbyLabel?.text = personalID
        
        lose_phone_textfield?.delegate = self
        
        
        var mobile_no = NODetail?["mobile_no"] as? String
         start = mobile_no?.index((mobile_no?.startIndex)!, offsetBy: 2);
         end = mobile_no?.index((mobile_no?.startIndex)!, offsetBy: 2 + 4);
//        mobile_no?.replaceSubrange(start!..<end!, with: "-XXXX-")
        
        phonenoLabel?.text = mobile_no
        registerDateLabel?.text = NODetail?["registration_date"] as? String
        typeLabel?.text = NODetail?["type"] as? String
        
        if(NODetail != nil)
        {
        if(NODetail?["active"] as! Bool)
        {
            statusLabel?.text = "ใช้งาน"
        }else{
            statusLabel?.text = "ไม่ใช้งาน"
        }
       
        if(NODetail?["verified"] as! Bool)
        {
            verifiredLabel?.text = "ยืนยัน"
        }else{
            verifiredLabel?.text = "ไม่ยืนยัน"
        }
        
        if(NODetail?["is_suspicious"] as? Bool)!
        {
            self.report_btn?.setTitle("ยกเลิกแจ้งเบอร์แปลกปลอม", for: .normal)
            self.report_btn?.setBackgroundImage(UIImage.init(named: "red_bg"), for: .normal)
        }else{
            self.report_btn?.setTitle("แจ้งเบอร์แปลกปลอม", for: .normal)
            self.report_btn?.setBackgroundImage(UIImage.init(named: "blue_bg"), for: .normal)
        }
        
        if(NODetail?["is_main_no"] as? Bool)!
        {
//            self.report_btn.isHidden = true;
            self.report_btn?.setTitleColor(UIColor.init(colorLiteralRed: 168/255, green: 168/255, blue: 168/255, alpha: 1.0), for: .normal)
            self.report_btn?.setTitle("แจ้งเบอร์แปลกปลอม", for: .normal)
             self.report_btn?.setBackgroundImage(UIImage.init(named: "grey_bg"), for: .normal)
             self.report_btn?.isEnabled = false
        }
        }



        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
 */
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       let vc = segue.destination
    
    if(vc is PasscodeViewController)
    {
       (vc as! PasscodeViewController).delegate = self
        (vc as! PasscodeViewController).isOneTimeMode = true;
         (vc as! PasscodeViewController).authStep = 0;
         (vc as! PasscodeViewController).askOldPass = false;
         (vc as! PasscodeViewController).requireBacktoLogout = true
        (vc as! PasscodeViewController).requireBack = true
    }
    }
 

}













