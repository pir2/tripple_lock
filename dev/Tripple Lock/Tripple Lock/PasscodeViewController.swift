//
//  PasscodeViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 5/17/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

protocol PasscodeDelegate {
    func recheckOldPasscode(code : String) -> Bool
    func successPassCode(code : String )
    func failedPassCode()
    
}


import UIKit
import Alamofire
import RxSwift
import RxAlamofire
import PopupDialog
import JSSAlertView
import KVLoading

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}

class PasscodeViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    public var delegate:PasscodeDelegate? = nil
    
    private var collectNumber : [Int]!  = [Int](repeating: 0, count: 5)
    private var collectRecheckNumber : [Int]!  = [Int](repeating: 0, count: 5)
    private var collectOldNumber : [Int]!  = [Int](repeating: 0, count: 5)
    private var index:Int = 0;
    public var isDubbleAuth:Bool = true
    public var askOldPass:Bool = false
    public var authStep:Int = 0
    public var isOneTimeMode:Bool = false;
    public var isGetPasscodeOnly:Bool = false;
    public var isOneTimeUseMode:Bool = false;
    public var isSetNewAuth:Bool = true;
    public var isRequireForget:Bool = true;
    public var requireBacktoLogout:Bool = false;
    public var requireBack:Bool = false;

    
    public var titleString:[String] = [];
    public var subTitleString:[String] = [];
    @IBOutlet weak var forgotBTN: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    @IBAction func touchBack(_ sender: Any) {
        if(requireBacktoLogout)
        {
            if(requireBack)
            {
                if(self.navigationController != nil)
                {
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }else{
                
            
            
            let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
            let alertview = JSSAlertView().show(self,
                                                title: "ออกจากระบบ",
                                                text: "คุณต้องการออกจากระบบใช่หรือไม่" ,
                                                buttonText: "ใช่",
                                                cancelButtonText:"ไม่",
                                                iconImage: customIcon)
            
            
            alertview.setTitleFont("Athiti-Medium")
            alertview.setTextFont("Athiti-Medium")
            alertview.setButtonFont("Athiti-Medium")
            alertview.setTextTheme(.dark)
            alertview.addAction({
                UserDefaults.standard.removeObject(forKey: "token")
                UserDefaults.standard.synchronize();
                
                UserDefaults.standard.removeObject(forKey: "token")
                UserDefaults.standard.synchronize();
                
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "welcome")
                UIApplication.shared.keyWindow?.rootViewController = viewController

            })
            alertview.addCancelAction({ 
                
            })
            }

            
        }else{
            if(self.navigationController != nil)
            {
                self.navigationController?.popViewController(animated: true)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBOutlet weak var progressStack: UIStackView!
    
    @IBOutlet weak var passcodeTitleLabel: UILabel!
    @IBOutlet weak var passcodeSubTitleLabel: UILabel!
    
    @IBAction func onTouchForgot(_ sender: Any) {
        self.performSegue(withIdentifier: "forgotpassword", sender: nil)
    }
    @IBOutlet weak var deleteButton: UIButton!
    
    var wrongTypeFailed:Int = 0;
    var isLock:Bool = false
    var countDown:Int = 0;
    var timer:Timer!
    
    
    @IBAction func touchNumber(_ sender: Any) {
        
        if(!isLock) {
        if(index < collectNumber.count)
        {
            
            let tag = (sender as AnyObject).tag
            if(authStep == -1){
                
                if(tag == 10){
                    collectOldNumber[index]  = 0
                }else{
                    collectOldNumber[index]  = tag!
                }
            }else if(authStep==0){
                if(tag == 10){
                    collectNumber[index]  = 0
                }else{
                    collectNumber[index]  = tag!
                }
            } else{
                if(tag == 10){
                    collectRecheckNumber[index]  = 0
                }else{
                    collectRecheckNumber[index]  = tag!
                }
            }
            
            let circle = progressStack.subviews[index] as! UIImageView
            circle.image = UIImage(named: "fill_small_circle")
            
            index += 1
            
            if #available(iOS 10.0, *) {
                Timer.scheduledTimer(withTimeInterval: 0.01, repeats: false, block: { (timer) in
                    self.checkStep();
                })
            } else {
                Timer.init(timeInterval: 0.01, target: self, selector: #selector(checkStep), userInfo: nil, repeats: false).fire()
            }
            
            
        }
        
        checkDeleteStatus();
        
        if(!isDubbleAuth && !isOneTimeUseMode)
        {
            self.passcodeTitleLabel.text = "ระบุรหัสผ่าน"
            self.passcodeSubTitleLabel.text = "เพื่อยืนยัน"
            
        }
        }
    }
    
    func savePasscode()
    {
        var  present_passcode : String = ""
        collectNumber.map({ (val ) in
            let code = val.description
            present_passcode.append(code)
        })
        //        Update local passcode
        
//        UserDefaults.standard.set(present_passcode, forKey: "passcode")
//        UserDefaults.standard.synchronize()
        //        Request save passcode on server.
        
        KVLoading.show()
        
        let personalID : String = UserDefaults.standard.string(forKey: "personalID")!
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"save_passcode","id":personalID  ,"passcode":present_passcode,"device_token":UIDevice.current.identifierForVendor!.uuidString ] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                let app = UIApplication.shared.delegate as! AppDelegate
                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
//                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = (error["message"]  as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                        }))
                        alert.addAction(UIAlertAction.init(title: "Retry", style: .default, handler: { (action) in
                            self?.savePasscode()
                        }))
                        self?.present(alert, animated:true, completion: nil)
                        
                        
                        
                        
                        self?.index = 0;
                        self?.authStep = 0;
                        self?.collectNumber =  [Int](repeating: 0, count: 5)
                        self?.collectRecheckNumber =  [Int](repeating: 0, count: 5)
                        
                        self?.setNewAuthState();
                        self?.cleanStatus();
                          self?.checkDeleteStatus();
                    }else{
                        
                        UserDefaults.standard.set(present_passcode, forKey: "passcode")
                        UserDefaults.standard.synchronize()

                        self?.doPassCodeComplete()
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
    }
    
    
    func cleanStatus()
    {
        progressStack.subviews.forEach { (view) in
            let  im : UIImageView =  view as! UIImageView
            
            im.image = UIImage(named: "empty_small_circle")
            
        }
    }
    
    func setNewAuthState()
    {
        self.passcodeTitleLabel.text = "ตั้งรหัสผ่านใหม่"
        self.passcodeSubTitleLabel.text = "เพื่อเปลี่ยนรหัส"
    }
    func setOldAuthState()
    {
        self.passcodeTitleLabel.text = "ระบุรหัสเก่า"
        self.passcodeSubTitleLabel.text = "เพื่อเปลี่ยนรหัส"
    }
    
    func setOneTimeTitle()
    {
        self.passcodeTitleLabel.text = "ระบุรหัสผ่าน"
        self.passcodeSubTitleLabel.text = "เพื่อเข้าระบบ"
    }
    
    func setOneTimeUseTitle()
    {
        self.passcodeTitleLabel.text = "ระบุรหัสผ่าน"
        self.passcodeSubTitleLabel.text = "เพื่ออนุมัติการทำงาน"
    }
    
    
    func setComfirmAuthState()
    {
        self.passcodeTitleLabel.text = "ตั้งรหัสอีกครั้ง"
        self.passcodeSubTitleLabel.text = "เพื่อยืนยัน"
    }
    
    func getDelayTime(target:Int)->Int
    {
        var delay = 0;
        switch target {
        case 3:
            delay = 15
            break;
        case 5:
            delay = 30
            break;
        case 7:
            delay = 60
            break;
        case 10:
            delay = 300
            break;
        case 15:
            delay = 60 * 15
            break;
        default:
            delay = 60 * 15;
            break
        }
        
        return delay
    }
    
    func updateTimeLeft()
    {
        countDown -= 1
        let min =  countDown % 60
        
        if(countDown>60)
        {
            passcodeSubTitleLabel.text = "เหลือเวลาอีก " + String(countDown/60) + " นาที " + String(min) + " วินาที"
        }else{
        passcodeSubTitleLabel.text = "เหลือเวลาอีก " + String(countDown) + " วินาที"
        }
        
        if(countDown<=0)
        {
            isLock = false
            passcodeTitleLabel.text = "ใส่รหัส"
            passcodeSubTitleLabel.text  = "เพื่อปลดล็อค"
            
            
            if(isOneTimeMode)
            {
                self.backButton.isHidden = true;
            }else if(isOneTimeUseMode)
            {
                self.backButton.isHidden = false;
            }
            
            
            timer.invalidate()
            
        }
    }
    
    func alertTimeout()
    {
//        let popup = PopupDialog.init(title: "", message: "คุณไม่ได้ทำรายการในเวลาที่กำหนด")
//        popup.buttonAlignment = .horizontal
//        popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
//            popup.dismiss()
//            
//            self.dismiss(animated: true, completion: nil)
//        }))
//        
//        self.present(popup, animated: true, completion: nil)
        
        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
        let alertview = JSSAlertView().show(self,
                                            title: "",
                                            text: "คุณไม่ได้ทำรายการในเวลาที่กำหนด",
                                            buttonText: "ปิด",
                                            iconImage: customIcon)
        
        
        alertview.setTitleFont("Athiti-Medium")
        alertview.setTextFont("Athiti-Medium")
        alertview.setButtonFont("Athiti-Medium")
        alertview.setTextTheme(.dark)
    }
    
    func checkStep()
    {
        if(index >= collectNumber.count  && isGetPasscodeOnly)
        {
             self.doPassCodeComplete()
        }else if(index >= collectNumber.count  && (isOneTimeMode || isOneTimeUseMode))
        {
            
            let local_passcode : String = UserDefaults.standard.string(forKey: "passcode")! //(UserDefaults.standard.object(forKey: "passcode") as? String)!
            var  present_passcode : String = "";
            
            for val in collectNumber{
           
                let code = String(val);
                present_passcode.append(code)
            }
            
            if(local_passcode == present_passcode)
            {
                print ("equal value")
                UserDefaults.standard.set(0, forKey: "wrongTypeFailed")
                UserDefaults.standard.synchronize()
                
                 self.doPassCodeComplete()
                
                if(self.navigationController != nil)
                {
                    self.navigationController?.popViewController(animated: true)
                }else{
                self.dismiss(animated: true, completion: nil)
                }
               
            }else{
                print("not  equal")
                
                wrongTypeFailed += 1
                
                if(wrongTypeFailed == 3 || wrongTypeFailed == 5 || wrongTypeFailed == 7 || wrongTypeFailed == 10 || wrongTypeFailed == 13 || wrongTypeFailed > 13 )
                {
                    
                    let delay = getDelayTime(target: wrongTypeFailed)
                    
                    let calendar = Calendar.current
                   
                    let date = calendar.date(byAdding: .minute, value: 5, to: Date())
                    
                    UserDefaults.standard.set(date?.millisecondsSince1970, forKey: "lockTime")
                     UserDefaults.standard.set(wrongTypeFailed, forKey: "wrongTypeFailed")
                    UserDefaults.standard.synchronize()
                    
                    
                    print(String(describing: date?.millisecondsSince1970) + " compaire " + String(Date().millisecondsSince1970))
                  
                    
                    countDown = delay
                    
                    let min =  countDown % 60
                    
                    var alertMessage = "";
                    
                    if(countDown>60)
                    {
                        alertMessage =   String(countDown/60) + " นาที " + (min == 0 ? "" : String(min) + " วินาที")
                    }else{
                        alertMessage =  String(countDown) + " วินาที"
                    }

                    
                    
                    
//                    let popup = PopupDialog.init(title: "", message: "ระบุรหัสผ่านผิด " + String(wrongTypeFailed) + " ครั้ง คุณจะสามารถ กรอกได้อีกครั้งภายใน " + alertMessage)
//                    popup.buttonAlignment = .horizontal
//                    
//                        popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
//                        popup.dismiss()
//                        self.index = 0;
//                        self.collectNumber = [Int](repeating: 0, count: 5)
//                        self.cleanStatus();
//                        self.checkDeleteStatus();
//                    }))
                      DispatchQueue.main.async {
//                    let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
//                    let alertview = JSSAlertView().show(self,
//                                                        title: "",
//                                                        text: "ระบุรหัสผ่านผิด " + String(self.wrongTypeFailed) + " ครั้ง คุณจะสามารถ กรอกได้อีกครั้งภายใน " + alertMessage,
//                                                        buttonText: "ปิด",
//                                                        iconImage: customIcon)
//                    
//                    
//                    alertview.setTitleFont("Athiti-Medium")
//                    alertview.setTextFont("Athiti-Medium")
//                    alertview.setButtonFont("Athiti-Medium")
//                    alertview.setTextTheme(.dark)
//                    alertview.addAction({ 
//                        self.index = 0;
//                        self.collectNumber = [Int](repeating: 0, count: 5)
//                        self.cleanStatus();
//                        self.checkDeleteStatus();
//                    })
                        
                        
                        let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "wraning_alert") as! StepAlertViewController
                        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        
                        myAlert.messageText = "ระบุรหัสผ่านผิด " + String(self.wrongTypeFailed) + " ครั้ง คุณจะสามารถ กรอกได้อีกครั้งภายใน " + alertMessage
                        myAlert.touchNagative {
                            self.index = 0;
                            self.collectNumber = [Int](repeating: 0, count: 5)
                            self.cleanStatus();
                            self.checkDeleteStatus();
                        }
                        
                        self.present(myAlert, animated: true, completion: {
                            
                          
                        })
                        
                        
                    }
                    
                    
                    
                    
                    
                    self.passcodeTitleLabel.text = "ล็อค"
                    self.passcodeSubTitleLabel.text = "รออีก " +  String(delay)
                    
                    isLock = true;
                   
                    self.backButton.isHidden = true;
                    
                    timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeLeft), userInfo: nil, repeats: true)
                    
//                    DispatchQueue.main.async {
//                     //   self.present(popup, animated: true, completion: nil)
//                    }
                }else{
//                    let popup = PopupDialog.init(title: "", message: "ระบุรหัสผ่านผิด")
//                    popup.buttonAlignment = .horizontal
//                    
//                    popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
//                        popup.dismiss()
//                        self.index = 0;
//                        self.collectNumber = [Int](repeating: 0, count: 5)
//                        self.cleanStatus();
//                        self.checkDeleteStatus();
////                        self.delegate?.failedPassCode()
//                    }))
//                    
//                    DispatchQueue.main.async {
//                        self.present(popup, animated: true, completion: nil)
//                    }
                    
                    DispatchQueue.main.async {
//                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
//                        let alertview = JSSAlertView().show(self,
//                                                            title: "",
//                                                            text: "ระบุรหัสผ่านผิด" ,
//                                                            buttonText: "ปิด",
//                                                            iconImage: customIcon)
//                        
//                        
//                        alertview.setTitleFont("Athiti-Medium")
//                        alertview.setTextFont("Athiti-Medium")
//                        alertview.setButtonFont("Athiti-Medium")
//                        alertview.setTextTheme(.dark)
//                        alertview.addAction({
//                            self.index = 0;
//                                                    self.collectNumber = [Int](repeating: 0, count: 5)
//                                                    self.cleanStatus();
//                                                    self.checkDeleteStatus();
//                                                    self.delegate?.failedPassCode()                        })
                        
                        let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "wraning_alert") as! StepAlertViewController
                        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        myAlert.messageText = "ระบุรหัสผ่านผิด"
                        myAlert.touchNagative {
                            self.index = 0;
                            self.collectNumber = [Int](repeating: 0, count: 5)
                            self.cleanStatus();
                            self.checkDeleteStatus();
                            self.delegate?.failedPassCode()
                        }
                       
                        self.present(myAlert, animated: true, completion: { 
                           
                            myAlert.titleLabel.text = ""
                        })
                    }


                }
            }
            
        }else if(index >= collectNumber.count  && authStep == -1 && isDubbleAuth)
        {
           
            
            let local_passcode : String = UserDefaults.standard.string(forKey: "passcode")! //(UserDefaults.standard.object(forKey: "passcode") as? String)!
            var  present_passcode : String = ""
            
            for val in collectOldNumber
            {
                let code = String(val);
                present_passcode.append(code)
            }
            
            if(local_passcode == present_passcode)
            {
                print ("equal value")
                index = 0;
                authStep = 0;
                 self.checkDeleteStatus();
                self.setNewAuthState();
                self.cleanStatus();
            }else{
                print("not  equal")
                
                
                
//                let alert = UIAlertController.init(title: "", message: "ระบุรหัสผ่านผิด", preferredStyle: .alert)
//                alert.addAction(UIAlertAction.init(title: "ตกลง", style: .default, handler: { (action) in
//                    alert.dismiss(animated: true, completion: nil);
//                }))
                
//                let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
//                let alertview = JSSAlertView().show(self,
//                                                    title: "",
//                                                    text: "ระบุรหัสผ่านผิด" ,
//                                                    buttonText: "ปิด",
//                                                    iconImage: customIcon)
//                
//                
//                alertview.setTitleFont("Athiti-Medium")
//                alertview.setTextFont("Athiti-Medium")
//                alertview.setButtonFont("Athiti-Medium")
//                alertview.setTextTheme(.dark)
                
                
                let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "wraning_alert") as! StepAlertViewController
                myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                myAlert.messageText = "ระบุรหัสผ่านผิด"
                self.present(myAlert, animated: true, completion: {
                
                  
                    myAlert.titleLabel.text = ""
                })
                
                self.index = 0;
                self.collectNumber = [Int](repeating: 0, count: 5)
                self.cleanStatus();
                 self.checkDeleteStatus();
//                present(alert, animated: true, completion: nil);
            }

            
           
        }else if(index >= collectNumber.count  && authStep == 0 && isDubbleAuth)
        {
            index = 0;
            authStep = 1;
             self.checkDeleteStatus();
            self.setComfirmAuthState();
            self.cleanStatus();
            
            self.backButton.isHidden = true;
            timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(alertTimeout), userInfo: nil, repeats: false)
           
        }else if(index >= collectRecheckNumber.count  && authStep == 1 && isDubbleAuth)
        {
            if(compaireValue())
            {
                timer.invalidate();
                timer = nil
                if(isSetNewAuth)
                {
                    self.savePasscode();
                }else{
                     self.savePasscode();
                }
            }else{
               
                wrongTypeFailed += 1
                
                if(wrongTypeFailed >= 3)
                {
//                    let popup = PopupDialog.init(title: "", message: "ระบุรหัสผ่านผิดเกิน 3 ครั้ง กรุณาติดต่อเจ้าหน้าที่")
//                    popup.buttonAlignment = .horizontal
//                    popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
//                        popup.dismiss()
//                        
//                        self.index = 0;
//                        self.authStep = 0;
//                        self.setComfirmAuthState();
//                        self.cleanStatus();
//                        self.checkDeleteStatus();
//                        
//                        self.dismiss(animated: true, completion: { 
//                            
//                        })
//                    }))
//                    
//                    self.present(popup, animated: true, completion: nil)
                    
//                    let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
//                    let alertview = JSSAlertView().show(self,
//                                                        title: "",
//                                                        text: "ระบุรหัสผ่านผิดเกิน 3 ครั้ง กรุณาติดต่อเจ้าหน้าที่" ,
//                                                        buttonText: "ปิด",
//                                                        iconImage: customIcon)
//                    
//                    
//                    alertview.setTitleFont("Athiti-Medium")
//                    alertview.setTextFont("Athiti-Medium")
//                    alertview.setButtonFont("Athiti-Medium")
//                    alertview.setTextTheme(.dark)
//                    alertview.addAction({ 
//                        self.index = 0;
//                        self.authStep = 0;
//                        self.setComfirmAuthState();
//                        self.cleanStatus();
//                        self.checkDeleteStatus();
//                        
//                        self.dismiss(animated: true, completion: {
//                            
//                        })
//
//                    })
                    
                    
                    let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "wraning_alert") as! StepAlertViewController
                    myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    myAlert.messageText = "ระบุรหัสผ่านผิดเกิน 3 ครั้ง กรุณาติดต่อเจ้าหน้าที่"
                    myAlert.touchNagative {
                        self.index = 0;
                        self.authStep = 0;
                        self.setComfirmAuthState();
                        self.cleanStatus();
                        self.checkDeleteStatus();
                        
                        if(self.navigationController != nil)
                        {
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    
                    self.present(myAlert, animated: true, completion: {
                        
                        myAlert.titleLabel.text = ""
                    })
                    
                    
                }else{
//                    let popup = PopupDialog.init(title: "", message: "ระบุรหัสผ่านผิด")
//                    popup.buttonAlignment = .horizontal
//                    popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
//                        popup.dismiss()
//                        
//                        self.index = 0;
//                        self.authStep = 0;
//                        self.setComfirmAuthState();
//                        self.cleanStatus();
//                        self.checkDeleteStatus();
//                    }))
//                    
//                    self.present(popup, animated: true, completion: nil)
                    
//                    let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
//                    let alertview = JSSAlertView().show(self,
//                                                        title: "",
//                                                        text: "ระบุรหัสผ่านผิด" ,
//                                                        buttonText: "ปิด",
//                                                        iconImage: customIcon)
//                    
//                    
//                    alertview.setTitleFont("Athiti-Medium")
//                    alertview.setTextFont("Athiti-Medium")
//                    alertview.setButtonFont("Athiti-Medium")
//                    alertview.setTextTheme(.dark)
//                    alertview.addAction({
//                        self.index = 0;
//                        self.authStep = 0;
//                        self.setComfirmAuthState();
//                        self.cleanStatus();
//                        self.checkDeleteStatus();
//                    })
                    
                    let myAlert = self.storyboard?.instantiateViewController(withIdentifier: "wraning_alert") as! StepAlertViewController
                    myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    myAlert.messageText = "ระบุรหัสผ่านผิด"
                    myAlert.touchNagative {
                        self.index = 0;
                        self.collectNumber = [Int](repeating: 0, count: 5)
                        self.cleanStatus();
                        self.checkDeleteStatus();
                        self.delegate?.failedPassCode()
                    }
                    
                    self.present(myAlert, animated: true, completion: {
                        
                        myAlert.titleLabel.text = ""
                    })
                }
            }
            
            
        }else if(index >= collectNumber.count  && !isDubbleAuth)
        {
            doPassCodeComplete()
        }
    }
    
    var beforeStep:Int = 0
    
    override func viewDidAppear(_ animated: Bool) {
        beforeStep = authStep
    }
    
    
    func reset()
    {
        index = 0;
        authStep = beforeStep;
         self.checkDeleteStatus();
        collectNumber = [Int](repeating: 0, count: 5)
        collectRecheckNumber = [Int](repeating: 0, count: 5)
        self.cleanStatus();
    }
    
    func doPasscodeNotComplete()
    {
//        print("Not Complete check passcode.")
//        let alert  = UIAlertController.init(title: "เกิดข้อผิดพลาด", message: "รหัสไม่ถูกต้อง", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "ตกลง", style: .default, handler: { (action) in
//            alert.dismiss(animated: true, completion: nil)
//            self.reset();
//        }))
//        
//        self.present(alert, animated: true, completion: nil);
        
        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
        let alertview = JSSAlertView().show(self,
                                            title: "เกิดข้อผิดพลาด",
                                            text: "รหัสไม่ถูกต้อง" ,
                                            buttonText: "ปิด",
                                            iconImage: customIcon)
        
        
        alertview.setTitleFont("Athiti-Medium")
        alertview.setTextFont("Athiti-Medium")
        alertview.setButtonFont("Athiti-Medium")
        alertview.setTextTheme(.dark)
        alertview.addAction({
            self.reset();
        })

        
        delegate?.failedPassCode()
        
    }
    
    func doPassCodeComplete()
    {
        print("Complete check passcode.")
        var  present_passcode : String = ""
        
        for val in collectNumber
        {
            let code = String(val);
            present_passcode.append(code)
        }
       

        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.locale = Locale.init(identifier: "th_TH");
        
        UserDefaults.standard.setValue(dateFormatter.string(from: Date.init()), forKey: "last_login")
        
        UserDefaults.standard.synchronize();
        
        delegate?.successPassCode(code: present_passcode)
    }
    
    func compaireValue()->Bool{
        var result = true
        for index in 0...collectNumber.count
        {
            if(index<collectNumber.count && index < collectRecheckNumber.count)
            {
                if(collectNumber[index] != collectRecheckNumber[index])
                {
                    result = false
                }}
        }
        
        return result
    }
    
    func checkDeleteStatus()
    {
        if(index<=0)
        {
            deleteButton.isHidden = true;
        }else
        {
            deleteButton.isHidden = false;
        }
    }
    
    @IBAction func touchDelete(_ sender: Any) {
        
        let circle = progressStack.subviews[index-1] as! UIImageView
        circle.image = UIImage(named: "empty_small_circle")
        
        index -= 1
        
        checkDeleteStatus()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.cleanStatus();
        
        self.forgotBTN.isHidden = !isRequireForget
        
        if(askOldPass){
            self.setOldAuthState()
        }else if(isOneTimeMode)
        {
            self.backButton.isHidden = true;
            self.setOneTimeTitle()
        }else if(isOneTimeUseMode)
        {
            self.backButton.isHidden = false;
            self.setOneTimeUseTitle()
        }
            
        if(requireBacktoLogout)
        {
             self.backButton.isHidden = false;
        }
        
       
        
        let mili = Int64(UserDefaults.standard.integer(forKey: "lockTime"))
        let currentTime = Int64(Date().timeIntervalSince1970 * 1000)
        
        if(mili > currentTime)
        {
            print("is still lock")
            wrongTypeFailed = UserDefaults.standard.integer(forKey: "wrongTypeFailed")
            let delay:Int64 = (mili - Date().millisecondsSince1970) / 1000
            countDown = Int(delay)
            self.passcodeTitleLabel.text = "ล็อค"
            self.passcodeSubTitleLabel.text = "รออีก " +  String(delay)
            
            isLock = true;
            
            self.backButton.isHidden = true;
            
           timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimeLeft), userInfo: nil, repeats: true)
            
           
            
        }else{
            print("Free")
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
