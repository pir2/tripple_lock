//
//  SuggestViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 6/15/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit

class SuggestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onTouckBack(_ sender: Any) {
//        dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func onTouchApp(_ sender: Any) {
//
        if let url = URL(string: "https://itunes.apple.com/th/app/%E0%B8%81-%E0%B8%99%E0%B8%81%E0%B8%A7%E0%B8%99/id1222833133?mt=8") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:])
            } else {
                UIApplication.shared.openURL(URL(string: "http://prepaid.nbtc.go.th/")!)
            }
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
