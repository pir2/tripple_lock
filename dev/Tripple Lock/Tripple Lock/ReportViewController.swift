//
//  ReportViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import KVLoading

class ReportViewController: UIViewController {

    @IBOutlet weak var operatorImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
(UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        // Do any additional setup after loading the view.
        
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.onTapView(gesture:)))
               self.operatorImage.addGestureRecognizer(tapGesture)
    }
    
    func onTapView(gesture:UITapGestureRecognizer)
    {
        print(gesture.view?.tag ?? "notfound")
        if(gesture.view?.tag == 2)
        {
            self.performSegue(withIdentifier: "operatorDetail", sender: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchBack(_ sender: Any) {
        self.dismiss(animated: true, completion:  nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "operatorDetail")
        {
            let operatorDetail:OperatorDetaileViewController = segue.destination as! OperatorDetaileViewController
            operatorDetail.isReport = true;
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
