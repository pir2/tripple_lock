//
//  AlertViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 6/8/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import PopupDialog
import JSSAlertView

enum AlertCustom {
    case warning
    case success
    case failed
    case disable_number
}
class AlertViewController: UIViewController {

    var popup  :PopupDialog?
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    public func show(title : String , message:String,type:AlertCustom)->PopupDialog
    {
        var icon :UIImage?
        switch type {
        case .warning:
            setWarningAppearance()
            icon = UIImage.init(imageLiteralResourceName: "alert_ic");
            break;
        case .success:
            setSuccessAppearance()
            icon = UIImage.init(imageLiteralResourceName: "complete_ic");
            break;
        case .failed:
            setWarningAppearance()
            icon = UIImage.init(imageLiteralResourceName: "alert_ic");
            break;
        case .disable_number:
            setWarningAppearance()
            icon = UIImage.init(imageLiteralResourceName: "disable_number_ic");
            break;
        default: break
            
        }
        
        
//        var alertview : JSSAlertViewResponder  = JSSAlertView().show(
//            target,
//            title: title,
//            text: message,
//            color: UIColorFromHex(0x71757A, alpha: 1),
//            iconImage: icon)
//        
//        
//        alertview.setTextFont("Athiti-Medium")
//        alertview.setButtonFont("Athiti-Medium")
//        alertview.setTitleFont("Athiti-Medium")
//        alertview.setTextTheme(.light)
        popup  = PopupDialog.init(title: title, message: message, image: icon)
        
        return  popup!
        
    }
    
    func setWarningAppearance()
    {
        let dialogAppearance = PopupDialogDefaultView.appearance()
        
        dialogAppearance.backgroundColor      = UIColor.white
        dialogAppearance.titleFont            =  UIFont.init(name: "Athiti-Medium", size: 29.0)!
        dialogAppearance.titleColor           = UIColor(rgb: 0x222527)
        dialogAppearance.titleTextAlignment   = .center
        dialogAppearance.messageFont          = UIFont.init(name: "Athiti-Medium", size: 19.0)!
        dialogAppearance.messageColor         = UIColor(rgb: 0x575E62)
        dialogAppearance.messageTextAlignment = .center
      
    }
    
    func setSuccessAppearance()
    {
        let dialogAppearance = PopupDialogDefaultView.appearance()
        
        dialogAppearance.backgroundColor      = UIColor.white
        dialogAppearance.titleFont            =  UIFont.init(name: "Athiti-Medium", size: 29.0)!
        dialogAppearance.titleColor           = UIColor(rgb: 0xC81924)
        dialogAppearance.titleTextAlignment   = .center
        dialogAppearance.messageFont          = UIFont.init(name: "Athiti-Medium", size: 19.0)!
        dialogAppearance.messageColor         = UIColor(rgb: 0x71757A)
        dialogAppearance.messageTextAlignment = .center
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}

