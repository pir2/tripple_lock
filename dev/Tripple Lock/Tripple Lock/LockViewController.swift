//
//  LockViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 2/16/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire
import PopupDialog
import JSSAlertView
import KVLoading

class LockViewController: UIViewController,PasscodeDelegate {
    
    let disposeBag = DisposeBag()
    
    var alertTimer:Timer!
    var popup:PopupDialog!
    var countdownTime:Timer!
    
    func successPassCode(code: String) {
        
        
        let oldpasscode : String =   UserDefaults.standard.string(forKey: "passcode")!
        
        if(oldpasscode == code)
        {
            
            
//        self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
            
            
        lock_status = !lock_status
        self.updateStatus(status: lock_status)
        
        
        requestLockNumber(is_lock: lock_status)
        }else{
//            let  popup = PopupDialog.init(title: "รหัสไม่ถูกต้อง", message:"กรุณาระบุรหัสใหม่อีกครั้ง")
//            
//            popup.addButton(PopupDialogButton.init(title: "ปิด", action: {
//                popup.dismiss()
//            }))
//            
//            present(popup, animated: true, completion: nil)
            
            let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
            let alertview = JSSAlertView().show(self,
                                                title: "รหัสไม่ถูกต้อง",
                                                text: "กรุณาระบุรหัสใหม่อีกครั้ง" ,
                                                buttonText:"ปิด",
                                                
                                                iconImage: customIcon)
            
            
            alertview.setTitleFont("Athiti-Medium")
            alertview.setTextFont("Athiti-Medium")
            alertview.setButtonFont("Athiti-Medium")
            alertview.setTextTheme(.dark)
          
            
            
        }
    }

    func recheckOldPasscode(code: String) -> Bool {
        return true;
    }

    
    func requestLockNumber(is_lock : Bool)
    {
        let token  =   UserDefaults.standard.string(forKey: "token")
        var lockString = "true"
        
        if is_lock != true
        {
        lockString = "false"
        }
        
        KVLoading.show()
        
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"set_is_lock","access_token":token ?? "","is_lock":lockString,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
//                let app = UIApplication.shared.delegate as! AppDelegate
//                app.onResponseValue.value = json
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
//                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
//                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
//                            alert.dismiss(animated: true, completion: nil)
//                        }))
//                        self?.present(alert, animated:true, completion: nil)
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "",
                                                            text: message ,
                                                            buttonText:"ปิด",
                                                            
                                                            iconImage: customIcon)
                        
                        alertview.addAction {
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        
                    }else{
                        
                        let  message = dict["result"]!["message"]!
                        var title = "ล็อค"
                        
                        if(is_lock == false)
                        {
                            title = "ไม่ล็อค"
                        }
                        
                        UserDefaults.standard.set(is_lock, forKey: "is_lock")
                        UserDefaults.standard.synchronize()
                        
                        print("สถานะล็อคเบอร์ \(is_lock)")
                        
                        if(is_lock == true){
                            let customIcon = #imageLiteral(resourceName: "complete_ic")
                            let alertview = JSSAlertView().show(self!, title: title, text: message as? String, buttonText:"ปิด", iconImage: customIcon)
                                    alertview.setTitleFont("Athiti-Medium")
                                    alertview.setTextFont("Athiti-Medium")
                                    alertview.setButtonFont("Athiti-Medium")
                                    alertview.setTextTheme(.dark)
                                    alertview.addAction({
//                            self?.dismiss(animated: true, completion: nil)
                                    self?.navigationController?.popViewController(animated: true)
                                    })
                                alertview.addCancelAction({})
                        }else{
                            
                            self?.createAlertUnlock()
                            
                            
//                            let customIcon = #imageLiteral(resourceName: "complete_ic")
//                            let alertview = JSSAlertView().show(self!, title: title, text: message as? String, buttonText:"ปิด", iconImage: customIcon)
//                            alertview.setTitleFont("Athiti-Medium")
//                            alertview.setTextFont("Athiti-Medium")
//                            alertview.setButtonFont("Athiti-Medium")
//                            alertview.setTextTheme(.dark)
//                            alertview.addAction({
////                            self?.dismiss(animated: true, completion: nil)
//                                self?.navigationController?.popViewController(animated: true)
//
//                            })
//                            alertview.addCancelAction({})
                    
                        }
                        
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
    }
    
    func questLockNumber(is_lock : Bool)
    {
        
        KVLoading.show()
        let token  =   UserDefaults.standard.string(forKey: "token")
        var lockString = "true"
        
        if is_lock != true
        {
            lockString = "false"
        }
        
        RxAlamofire.requestJSON(.post,Config.server_url,parameters: ["action":"set_is_lock","access_token":token ?? "","is_lock":lockString,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()

                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = ((error["message"] as! NSDictionary)["message"]! as? String)!
                        
                        let customIcon = #imageLiteral(resourceName: "dialog_warning_ic")
                        let alertview = JSSAlertView().show(self!,
                                                            title: "",
                                                            text:  message ,
                                                            buttonText:"ปิด",
                                                            iconImage: customIcon)
                        
                        alertview.addAction {
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.onResponseValue.value = json
                        }
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        
                        
                    }else{
                        
                        let vc = UIApplication.topViewController()
//                        let  message = dict["result"]!["message"]!
                        let customIcon = #imageLiteral(resourceName: "lock_ic")
                        let alertview = JSSAlertView().show(vc!,
                                                            title: "ล็อค",
//                                                            text:  message as! String ,
                                                            text: "ระบบได้ทำการล็อคการเปิดเบอร์เรียบร้อยแล้ว",
                                                            buttonText:"ปิด",
                                                            iconImage: customIcon)
                        
                        
                        alertview.setTitleFont("Athiti-Medium")
                        alertview.setTextFont("Athiti-Medium")
                        alertview.setButtonFont("Athiti-Medium")
                        alertview.setTextTheme(.dark)
                        alertview.addAction {
                            print("สวัสดีครับ ทดสอบ")
                            
                            NotificationCenter.default.post(name: .lockStatus, object: self)
                        }
                        
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
        
    }
    
    func closeDialog()
    {
        popup.dismiss()
        self.questLockNumber(is_lock: true)
    }
    
    func createAlertUnlock(){
        popup = PopupDialog.init(title: "ปลดล็อคสำเร็จ", message: "ระบบจะทำการล็อคการเปิดเบอร์ใหม่ภายใน 5 นาที เพื่อป้องกันบุคคลอื่นแอบอ้าง นำบัตรประชาชนไปเปิดหมายเลขโทรศัพท์",image:UIImage.init(named: "ic_warning_width"))
        popup.buttonAlignment = .horizontal
        
        if #available(iOS 10.0, *) {
            self.countdownTime = Timer.scheduledTimer(withTimeInterval: 300, repeats: false, block: { (time) in
                self.questLockNumber(is_lock: true)
                self.popup.dismiss()
            })
        } else {
            self.countdownTime = Timer.scheduledTimer(timeInterval: 300, target: self, selector: #selector(closeDialog), userInfo: nil, repeats: false)
        }
        
        popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
            self.popup.dismiss()
            
        }))

        self.present(popup, animated: true,completion: nil)

    }
    

    @IBOutlet weak var lock_icon: UIButton!
    @IBOutlet weak var status_title_txt: UILabel!
    @IBOutlet weak var status_txt: UILabel!
    @IBOutlet weak var lock_btn: UIButton!
    @IBOutlet weak var desctionLabel: UILabel!
    
    
    
    var lock_status:Bool! = true
    var lockImage:UIImage!  = UIImage.init(imageLiteralResourceName: "lock_icon")
    var unlockImage:UIImage!  = UIImage.init(imageLiteralResourceName: "unlock_icon")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lock_status = UserDefaults.standard.bool(forKey: "is_lock")
        
        if(lock_status)
        {
            self.status_txt.text = "สถานนะ : ปัจจุบันล็อค";
            self.desctionLabel.text = "ท่านสามารถปลดล็อคเพื่อเปิดเบอร์ได้"
        }else{
            self.status_txt.text = "สถานนะ : ปัจจุบันไม่ล็อค";
            self.desctionLabel.text = "ท่านสามารถล็อคเพื่อรักษาสิทธิ์ในการเปิดเบอร์ใหม่ได้"
        }
       
 
        (UIApplication.shared.delegate as! AppDelegate).currentView = self;
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let is_lock = UserDefaults.standard.bool(forKey: "is_lock")
        updateStatus(status: is_lock)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchBack(_ sender: Any) {
//        self.dismiss(animated: true) { 
//            
//        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func updateStatus(status:Bool)
    {
        if(status)
        {
            status_txt.text = "ปลดล็อคการลงทะเบียน"
            lock_btn.setTitle("ปลดล็อค", for: UIControlState.normal)
            lock_icon.setImage(lockImage, for: UIControlState.normal)
        }else{
            status_txt.text = "ล็อคการลงทะเบียน"
            lock_btn.setTitle("ล็อค", for: UIControlState.normal)
             lock_icon.setImage(unlockImage, for: UIControlState.normal)
        }
        
        if(status)
        {
            self.status_txt.text = "สถานะ : ปัจจุบันล็อค";
            self.desctionLabel.text = "ท่านสามารถปลดล็อคเพื่อเปิดเบอร์ได้"
        }else{
            self.status_txt.text = "สถานะ : ปัจจุบันไม่ล็อค";
            self.desctionLabel.text = "ท่านสามารถล็อคเพื่อรักษาสิทธิ์ในการเปิดเบอร์ใหม่ได้"
        }
    }
//    PasscodeDelegate
    func successPassCode() {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
        lock_status = !lock_status
        self.updateStatus(status: lock_status)
        
        
    }
    func failedPassCode() {
        
    }
    

    @IBAction func touchLock(_ sender: Any) {
        
        self.performSegue(withIdentifier: "passcode", sender: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let vc = segue.destination
        if(vc is PasscodeViewController)
        {
           (vc as! PasscodeViewController).isDubbleAuth = false
            
            (vc as! PasscodeViewController).isOneTimeUseMode = true
            (vc as! PasscodeViewController).delegate = self
        }
    }
    

}
