//
//  RecoverPasswordViewController.swift
//  Tripple Lock
//
//  Created by PiR2 Developer MacPro on 5/23/2560 BE.
//  Copyright © 2560 PiR2 Developer MacPro. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import RxAlamofire
import PopupDialog
import KVLoading

class RecoverPasswordViewController: UIViewController ,UITextFieldDelegate,ReceiveOTPDelegate,PasscodeDelegate{
    func failedPassCode() {
        
    }

    func recheckOldPasscode(code: String) -> Bool {
        return true
    }


    let disposeBag  = DisposeBag();
    
    @IBOutlet weak var personalID: UITextField!
    @IBOutlet weak var mainTelephoneNumber: UITextField!
    @IBOutlet weak var email: UITextField!
    
    
    
    @IBAction func touchBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if(textField == mainTelephoneNumber)
        {
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 10
        }else
        {
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 13
        }
    }
    
    func validateField() -> Bool {
        var result  = true;
        if((self.personalID.text?.characters.count)! < 13)
        {
            result = false
            let popup = PopupDialog.init(title: "", message: "กรุณากรอกเลขบัตรประชาชน\nให้ครบ 13 หลัก")
            popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
                popup.dismiss()
            }))
            self.present(popup, animated: true, completion: nil)
        }
        else if((self.mainTelephoneNumber.text?.characters.count)! < 10)
        {
            result = false
            let popup = PopupDialog.init(title: "", message: "กรุณากรอกเบอร์โทรศัพท์\nให้ครบ 10 หลัก")
            popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
                popup.dismiss()
            }))
            self.present(popup, animated: true, completion: nil)
        }else if(self.email != nil)
        {
            
            if(!isValidEmail(testStr: (self.email?.text!)!))
        {
            let popup = PopupDialog.init(title: "", message: "กรุณากรอกอีเมลให้ถูกต้อง")
            popup.addButton(PopupDialogButton.init(title: "ตกลง", action: {
                popup.dismiss()
            }))
            self.present(popup, animated: true, completion: nil)

        }
        }
        
        
        return result;
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    
    @IBAction func touchCheck(_ sender: Any) {
        
        let email :String = (self.email.text!)
        let personalID:String = (self.personalID.text!)
        let phone :String = (self.mainTelephoneNumber.text!)
        
        if(validateField())
        {
        
            KVLoading.show()
            
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"forgot_password_email","id":personalID,"mobile":phone,"email":email,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                let app = UIApplication.shared.delegate as! AppDelegate
                app.onResponseValue.value = json
                
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = (error["message"]  as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }else{
                        
                        let message:String =  dict["result"]!["message"] as! String
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                            self?.dismiss(animated: true, completion: nil)
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)

        }
        
    }
    @IBAction func requestNewpassword(_ sender: Any) {
        
        let personalIDString:String = self.personalID.text!
        let phone :String = (self.mainTelephoneNumber.text)!
        
        if(validateField())
        {
        
            UserDefaults.standard.set(phone, forKey: "phone_otp")
            UserDefaults.standard.synchronize();
            
            KVLoading.show()
            
        RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"forgot_password_otp","id":personalIDString,"mobile":phone,"device_token":UIDevice.current.identifierForVendor!.uuidString] )
            .debug()
            .subscribe(onNext: { [weak self] (r, json) in
                
                KVLoading.hide()
                
                let app = UIApplication.shared.delegate as! AppDelegate
                app.onResponseValue.value = json
                
                
                if let dict = json as? [String: AnyObject] {
                    print(dict["result"]!["data"]!)
                    let status = dict["result"]!["status"] as! String
                    if(status == "error")
                    {
                        let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                        let message:String = (error["message"]  as? String)!
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                            alert.dismiss(animated: true, completion: nil);
                        }))
                        self?.present(alert, animated:true, completion: nil)
                    }else{
                        
                        let message:String =  dict["result"]!["message"] as! String
                        let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                           self?.performSegue(withIdentifier: "otp", sender: dict["result"]!["data"]!)
                            
                        }))
                        self?.present(alert, animated:true, completion: {
                         
                        })
                        
                    }
                }
                }, onError: { [weak self] (error) in
                    
            })
            .addDisposableTo(disposeBag)
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
(UIApplication.shared.delegate as! AppDelegate).currentView = self ;
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Receive OTP Delegate
    func failedOTP() {
        
    }
    func successOTP() {
        self.dismiss(animated: false) {
            self.performSegue(withIdentifier: "passcode", sender: nil)
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if(segue.destination is ReciveOTPViewController)
          {
       let vc = segue.destination as! ReciveOTPViewController
        vc.delegate = self;
        vc.isRegister = false
        let data = sender as! NSDictionary
        vc.refCode = data["reference"] as! String
          }else  if(segue.destination is PasscodeViewController){
            let vc = segue.destination as! PasscodeViewController
            vc.askOldPass = false
            vc.isRequireForget = true
            vc.isOneTimeMode = false
            vc.delegate = self
        }
    }
    
    
    func successPassCode(code : String) {
    
            UserDefaults.standard.set(code, forKey: "passcode")
            UserDefaults.standard.synchronize()
            
            KVLoading.show()
            
            let personalID : String = UserDefaults.standard.string(forKey: "personalID")!
            RxAlamofire.requestJSON(.post, Config.server_url,parameters: ["action":"save_passcode","id":personalID  ,"passcode":code,"device_token":UIDevice.current.identifierForVendor!.uuidString ] )
                .debug()
                .subscribe(onNext: { [weak self] (r, json) in
                    
                    KVLoading.hide()
                    let app = UIApplication.shared.delegate as! AppDelegate
                    app.onResponseValue.value = json
                    
                    
                    if let dict = json as? [String: AnyObject] {
                        print(dict["result"]!["data"]!)
                        let status = dict["result"]!["status"] as! String
                        if(status == "error")
                        {
                            let error : NSDictionary = dict["result"]!["error"]! as! NSDictionary
                            let message:String = (error["message"]  as? String)!
                            let alert:  UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction.init(title: "ปิด", style: .default, handler: { (action) in
                                alert.dismiss(animated: true, completion: nil);
                            }))
                            alert.addAction(UIAlertAction.init(title: "Retry", style: .default, handler: { (action) in
                               // self?.savePasscode()
                            }))
                            self?.present(alert, animated:true, completion: nil)
                            
                            
                        }else{
                            
//                            self?.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
                           
                            self?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
//                            self?.dismiss(animated: true, completion: {
//                                self?.dismiss(animated: true, completion: {
//                                    self?.dismiss(animated: true,completion:{
//                                        self?.dismiss(animated: true,completion:nil)
//                                         self?.dismiss(animated: false, completion: nil)
//                                    })
//                                })
//                            })
                            
                           
                        }
                    }
                    }, onError: { [weak self] (error) in
                        
                })
                .addDisposableTo(self.disposeBag)
            
            
            
        
        
    }
    
   
    

}
